//
// Created by stewart on 30/01/23.
//

#include "sunshine/common/ros_conversions.hpp"
#include <ros/ros.h>
#include <sunshine_msgs/TopicMap2D.h>
#include <sunshine_msgs/DenseTopicMap.h>
#include <sunshine_msgs/SetRegion.h>

#include "sunshine/common/data_proc_utils.hpp"
#include <opencv2/highgui/highgui.hpp>

#include <thread>

using namespace sunshine;
using namespace sunshine_msgs;

struct {
    bool operator()(std::pair<int, int> const& a, std::pair<int, int> const& b) const {
        return a.first < b.first || (a.first == b.first && a.second < b.second);
    }
} compareCoords;

class map_extrapolator {
    ros::NodeHandle* nh;
    ros::ServiceServer extrapolationService;
    std::string method;
//    double decay_distance;
    bool include_endpoints, showTopicMap, regionChanged = true;
    std::vector<double> extrapolation_origin;
    std::vector<double> extrapolation_size;
    double cell_width = 0.;
    uint32_t width = 0, height = 0, scaleTopicMap = 1;
    int32_t minX = 0, minY = 0, maxX = 0, maxY = 0;
//    std::vector<int> cell_poses;
    sunshine::WordColorMap<int> wordColorMap;
    ros::Subscriber map_sub;
    ros::Publisher map_pub;

    bool setRegion(sunshine_msgs::SetRegionRequest& request, sunshine_msgs::SetRegionResponse& response) {
        extrapolation_origin = {request.origin_x, request.origin_y};
        extrapolation_size = {request.size_x, request.size_y};
        response.success = true;
        regionChanged = true;
        return true;
    }

    void generateCellPoses() {
        if (cell_width <= 0.) throw std::logic_error("Cannot generate cell poses with invalid cell_width <= 0.!");
        ROS_INFO("Regenerating cell poses");
        width = static_cast<uint32_t>(std::ceil(this->extrapolation_size[0] / cell_width) + include_endpoints);
        height = static_cast<uint32_t>(std::ceil(this->extrapolation_size[1] / cell_width) + include_endpoints);
//        auto const toPoseOffset = [this](std::pair<int, int> coords) {
//            return ((coords.first - minX) * height + coords.second - minY) * 2;
//        };
//
//        cell_poses.resize(0);
//        cell_poses.reserve(width * height * 2);
        std::pair<int, int> const origin_cell = std::make_pair(std::round(this->extrapolation_origin[0] / cell_width), std::round(this->extrapolation_origin[1] / cell_width));
        minX = origin_cell.first;
        maxX = minX + width;
        minY = origin_cell.second;
        maxY = minY + height;
        regionChanged = false;
//        for (auto x = minX; x < maxX; ++x) {
//            for (auto y = minY; y < maxY; ++y) {
//                assert(toPoseOffset({x, y}) == cell_poses.size());
//                cell_poses.push_back(x);
//                cell_poses.push_back(y);
//            }
//        }
    }

    void callback(TopicMap2DConstPtr const& map) {
        auto start = std::chrono::steady_clock::now();
        assert(map->topic_map.cell_width[0] == map->topic_map.cell_width[1]);
        if (extrapolation_size[0] == 0 || extrapolation_size[1] == 0) {
            ROS_WARN("Extrapolation size is 0, waiting for it to be set via service call");
            return;
        } else if (map->topic_map.cell_width[0] <= 0.) {
            ROS_ERROR("Invalid new cell_width %f!", cell_width);
            return;
        } else if (map->topic_map.vocabulary_size <= 0) {
            ROS_WARN("K = %d, skipping", map->topic_map.vocabulary_size);
            return;
        } else if (regionChanged || cell_width != map->topic_map.cell_width[0]) {
            cell_width = map->topic_map.cell_width[0];
            generateCellPoses();
        }
        auto const setup = record_lap(start);

        DenseTopicMap extrapolated_map;
        extrapolated_map.source_node = ros::this_node::getName();
        extrapolated_map.info.origin.position.x = this->extrapolation_origin[0];
        extrapolated_map.info.origin.position.y = this->extrapolation_origin[1];
        extrapolated_map.info.resolution = map->topic_map.cell_width[0];
        extrapolated_map.info.width = width;
        extrapolated_map.info.height = height;
        extrapolated_map.header = map->header;
        extrapolated_map.spatial_cell_size = {map->topic_map.cell_width[0], map->topic_map.cell_width[1]};
        extrapolated_map.temporal_cell_size = map->topic_map.cell_time;
        extrapolated_map.map_shape = {width, height};
        extrapolated_map.K = map->topic_map.vocabulary_size;
        extrapolated_map.map_topic_proportions = std::vector<float>(width * height * extrapolated_map.K, 0.);
        extrapolated_map.map_valid_mask = std::vector<uint8_t>(width * height, 0);

        auto const& topics_begin = map->topic_map.cell_topics.cbegin();
        auto const& topics_end = map->topic_map.cell_topics.cend();

        auto const K = map->topic_map.vocabulary_size;
        auto const D = map->topic_map.cell_width.size();
        auto const N = map->topic_map.cell_poses.size() / D;

        bool const mlTopics = (N == map->topic_map.cell_topics.size() && K > 1) || (K == 1 && std::all_of(topics_begin, topics_end, [](int const& z){return z == 0;}));
        bool const fullDist = !mlTopics && (N * K == map->topic_map.cell_topics.size());
        if (!mlTopics && !fullDist) throw std::runtime_error("Unable to infer meaning of cell_topics");
        auto const M = (fullDist) ? K : 1;
        struct Pose {
            int x, y, z;
        };

        if (N == 0) {
            map_pub.publish(extrapolated_map);
            return;
        }

        auto const toMaskOffset = [this](std::pair<int, int> const& coords) {
            auto const v = ((coords.first - minX) * height + coords.second - minY);
            assert(v >= 0 && v < width * height);
            return v;
        };

        auto const toTopicOffset = [toMaskOffset, K](std::pair<int, int> const& coords) {
            return toMaskOffset(coords) * K;
        };

        auto const inBounds = [this](std::pair<int, int> const& coords) {
            return coords.first >= minX && coords.first < maxX && coords.second >= minY && coords.second < maxY;
        };

        std::map<std::pair<int, int>, std::pair<int, std::vector<float>>, decltype(compareCoords)> observed_topics(compareCoords), extrapolated_topics(compareCoords);
        std::vector<float> mean_dist(K, 0.f);
        uint64_t mean_weight = 0;

        auto const extractNormalized = [&mean_dist, &mean_weight, M, K, fullDist](auto begin, auto const& end){
            assert(std::distance(begin, end) == M);
            std::vector<float> normalized(K, 0.f);
            ++mean_weight;
            if (fullDist) {
                auto weight = std::accumulate(begin, end, 0.f);
                if (weight <= 0.) {
                    ROS_ERROR("Non-positive cell weight!");
                    weight = 1.;
                }
                for (auto k = 0; k < K; ++k) {
                    normalized[k] = *(begin + k) / weight;
                    mean_dist[k] += normalized[k];
                }
                for (auto k = 0; k < K; ++k) ROS_ERROR_COND(std::isnan(normalized[k]), "NaN topic proportion detected!");
            } else {
                assert(M == 1);
                auto const& k = *begin;
                normalized[k] = 1;
                ++(mean_dist[k]);
            }
            return normalized;
        };

        std::set<std::pair<int, int>, decltype(compareCoords)> frontier(compareCoords), next_frontier(compareCoords);

        auto const initialization = record_lap(start);

        for (auto i = 0; i < N; ++i) {
            Pose const* const pose = reinterpret_cast<Pose const*>(map->topic_map.cell_poses.data() + i * D);
            auto const& key = std::make_pair(pose->x, pose->y);
            if (!inBounds(key)) continue;
            frontier.insert(key);
            auto const topics = extractNormalized(topics_begin + i * M, topics_begin + (i + 1) * M);
            auto it = observed_topics.lower_bound(key);
            if (it != observed_topics.end() && it->first == key) {
                it->second.first++;
                ROS_DEBUG("Duplicate topics at pose [%d, %d], combining", pose->x, pose->y);
                std::transform(it->second.second.begin(), it->second.second.end(), topics.begin(), it->second.second.begin(), std::plus<>());
            } else {
                observed_topics.insert(it, {key, std::make_pair(1, topics)});
            }
        }

        for (auto const& e : observed_topics) {
            auto const offset = toMaskOffset(e.first);
            extrapolated_map.map_valid_mask[offset] = true;
        }

        auto const observed = record_lap(start);

        auto const neighbors = [](std::pair<int, int> coords){
            return std::array<std::pair<int, int>, 4>{{{coords.first, coords.second + 1},
                                                       {coords.first + 1, coords.second},
                                                       {coords.first, coords.second - 1},
                                                       {coords.first - 1, coords.second}}};
        };

        std::transform(mean_dist.begin(), mean_dist.end(), mean_dist.begin(), [mean_weight](auto const& v){
            return v / mean_weight;
        });
        mean_weight = 1;
        auto const extrapolate = [K](std::pair<int, std::vector<float>> const& observed, std::vector<float> const& base_dist){
            std::vector<float> extrapolated(K, 0.);
            auto const& obs_weight = observed.first;
            std::transform(observed.second.cbegin(), observed.second.cend(), base_dist.cbegin(), extrapolated.begin(), [obs_weight](auto const& a, auto const& b){
                return 5*a / obs_weight + b;
            });
            return extrapolated;
        };

//        for (auto i = 0; i < this->decay_distance; ++i) {
        while (!frontier.empty()) {
            for (auto const& e : frontier) {
                auto const obs = observed_topics[e];

                for (auto const& n : neighbors(e)) {
                    if (!inBounds(n)) continue;
                    if (observed_topics.find(n) != observed_topics.end()) continue;
                    next_frontier.insert(n);
                    auto it = extrapolated_topics.find(n);
                    if (it == extrapolated_topics.end()) extrapolated_topics.insert({n, {2, extrapolate(obs, mean_dist)}});
                    else {
                        it->second.second = extrapolate(obs, it->second.second);
                        it->second.first++;
                    }
                }
            }
            frontier = next_frontier;
            observed_topics.insert(extrapolated_topics.cbegin(), extrapolated_topics.cend());
            extrapolated_topics.clear();
            next_frontier.clear();
        }

        auto const extrapolation = record_lap(start);

        for (auto const& e : observed_topics) {
//            if (inBounds(e.first)) {
                auto const offset = toTopicOffset(e.first);
                auto begin = extrapolated_map.map_topic_proportions.begin() + offset;
                assert(e.second.first > 0);
                if (e.second.first == 1) {
                    auto end = std::copy(e.second.second.cbegin(), e.second.second.cend(), begin);
                    assert(end - begin == K);
                } else {
                    auto const weight = std::accumulate(e.second.second.begin(), e.second.second.end(), 0.);
                    for (auto i = 0; i < K; ++i) {
                        *begin = e.second.second[i] / weight;
                        begin++;
                    }
                }
//            }
        }

        auto const population = record_lap(start);

        assert(extrapolated_map.map_topic_proportions.size() == width * height * K);
        assert(extrapolated_map.map_valid_mask.size() == width * height);
        assert(extrapolated_map.info.width == width && extrapolated_map.info.height == height);
        map_pub.publish(extrapolated_map);

        auto const publish = record_lap(start);

        if (showTopicMap) {
            cv::Mat topicMapImg = sunshine::createTopicImg(extrapolated_map, wordColorMap, true, scaleTopicMap);
            cv::imshow(ros::this_node::getName(), topicMapImg);
            cv::waitKey(30);
        }
        auto const display = record_lap(start);
//        ROS_INFO("%lu ms setup, %lu ms initializing, %lu ms observed, %lu ms extrapolate, %lu populate, %lu publish, %lu display", setup, initialization, observed, extrapolation, population, publish, display);
    }

  public:
    explicit map_extrapolator(ros::NodeHandle* nh) : nh(nh) {
        extrapolationService = nh->advertiseService("set_region", &map_extrapolator::setRegion, this);
        this->method = nh->param<std::string>("method", "nearest-neighbor");
//        this->decay_distance = nh->param<double>("decay_distance", 1.);
        this->extrapolation_origin = split(nh->param<std::string>("origin", "0x0"), 'x', [](std::string const& s){ return std::stod(s); });
        this->extrapolation_size = split(nh->param<std::string>("size", "0x0"), 'x', [](std::string const& s){ return std::stod(s); });
        this->include_endpoints = nh->param<bool>("include_endpoints", false);
        this->scaleTopicMap = nh->param<int>("scale_extrapolated_map", 1);
        this->showTopicMap = nh->param<bool>("show_extrapolated_map", false);
        ROS_INFO("Will extrapolate using method '%s'", this->method.c_str());
        ROS_WARN_COND(include_endpoints, "The include endpoints setting is not recommended.");

        map_sub = nh->subscribe<TopicMap2D>("topic_map", 1, &map_extrapolator::callback, this);
        map_pub = nh->advertise<DenseTopicMap>("extrapolated_map", 1);
    }
};

int main(int argc, char** argv) {
    //    std::this_thread::sleep_for(std::chrono::seconds(5));
    ros::init(argc, argv, "map_extrapolator");
    ros::NodeHandle nh("~");

    map_extrapolator model(&nh);

    ros::SingleThreadedSpinner spinner;
    ROS_INFO("Spinning...");
    spinner.spin();
}