//
// Created by stewart on 12/01/23.
//

#include "sunshine/common/matching_utils.hpp"
#include "sunshine/common/segmentation_utils.hpp"
#include "sunshine/common/ros_conversions.hpp"
#include <ros/ros.h>
#include <sunshine_msgs/TopicMap2D.h>
#include <sunshine_msgs/TopicModel.h>

#include <thread>

using namespace sunshine;
using namespace sunshine_msgs;

template<typename T>
std::ostream & operator<<(std::ostream & os, std::vector<T> vec)
{
    os<<"[";
    if(vec.size()!=0)
    {
        std::copy(vec.begin(), vec.end()-1, std::ostream_iterator<T>(os, " "));
        os<<vec.back();
    }
    os<<"]";
    return os;
}

class map_unifier {
    ros::NodeHandle* nh;
    std::string match_method;
    std::vector<std::string> topics;
    bool publish_full_dist;
    ros::Publisher map_pub;
    std::vector<ros::Subscriber> map_sub;
    std::unordered_map<std::string, size_t> source_map;
    std::vector<Phi> models;
    std::vector<std::unique_ptr<Segmentation<std::vector<int>, 2, int, double>>> maps;
    match_results matches;
    std::optional<match_scores> scores;
    TopicMap2D global_map;

    void callback(TopicMap2DConstPtr const& map) {
        {
            auto const& key = map->source_node;
            auto model      = fromRosMsg(map->topic_model);
            if (model.K == 0) return;
            model.validate(true);
            // TODO: support std::vector<int> labels (perhaps provide a function for runtime check of TopicMap label type?)
            if (source_map.find(key) != source_map.end()) {
                ROS_DEBUG("Updating map and model from source %s", key.c_str());
                auto const idx = source_map[key];
                maps[idx]      = std::make_unique<Segmentation<std::vector<int>, 2, int, double>>(fromRosMsg<std::vector<int>>(*map));
                models[idx]    = model;
            } else {
                ROS_DEBUG("Adding new map and model from source %s", key.c_str());
                source_map.insert({key, maps.size()});
                maps.emplace_back(std::make_unique<Segmentation<std::vector<int>, 2, int, double>>(fromRosMsg<std::vector<int>>(*map)));
                models.emplace_back(model);
            }
        }

        ROS_INFO("Matching topics from %lu robots", source_map.size());
        matches = match_topics(match_method, models);
        scores  = match_scores(models, matches.lifting, cosine_distance<double>);
        ROS_INFO_STREAM("Unified model contains " << scores->K << " global topics of sizes " << scores->cluster_sizes << "."
                                                  << " DB=" << scores->davies_bouldin
                                                  << " MSCD=" << scores->mscd
                                                  << " SH=" << scores->silhouette);

        ROS_INFO("Merging and publishing global topic map");
        auto merged_segmentations = merge_segmentations<std::vector<int>>(maps, matches.lifting);
        if (merged_segmentations->observation_poses.size() != merged_segmentations->observations.size()) {
            ROS_ERROR("Merged map is inconsistent (# poses != # observations)! Check for race conditions. Aborting");
            return;
        }
        global_map = toRosMsg(*merged_segmentations, publish_full_dist);
        global_map.header.frame_id = ros::this_node::getName();
        global_map.header.stamp = ros::Time::now();
//        global_map.info = map->info; // TODO: check that the info matches in essential aspects, or else convert appropriately
        // TODO: do we want option to include fused model?
        map_pub.publish(global_map);
    }

  public:
    explicit map_unifier(ros::NodeHandle* nh) : nh(nh) {
        this->match_method = nh->param<std::string>("match_method", "clear-cos-0.75");
        this->topics = split(nh->param<std::string>("topics", "/topic_maps"), ' ');
        this->publish_full_dist = nh->param<bool>("publish_full_dist", false);
        ROS_INFO("Will match using method '%s'", this->match_method.c_str());

        map_pub   = nh->advertise<TopicMap2D>("/unified_map", 1);
        for (auto const& topic : topics) {
            map_sub.push_back(nh->subscribe<TopicMap2D>(topic, 1, &map_unifier::callback, this));
        }
    }
};

int main(int argc, char** argv) {
    //    std::this_thread::sleep_for(std::chrono::seconds(5));
    ros::init(argc, argv, "map_unifier");
    ros::NodeHandle nh("~");

    map_unifier model(&nh);

    ros::SingleThreadedSpinner spinner;
    ROS_INFO("Spinning...");
    spinner.spin();
}