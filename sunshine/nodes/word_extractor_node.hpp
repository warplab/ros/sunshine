//
// Created by stewart on 2020-07-06.
//

#ifndef SUNSHINE_PROJECT_VISUAL_WORD_EXTRACTOR_HPP
#define SUNSHINE_PROJECT_VISUAL_WORD_EXTRACTOR_HPP

#include "sunshine/depth_adapter.hpp"
#include "sunshine/common/ros_conversions.hpp"

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>

#include "ros/ros.h"
#include "cv_bridge/cv_bridge.h"
#include "sensor_msgs/image_encodings.h"
#include "image_transport/image_transport.h"

#include "nav_msgs/Odometry.h"
#include "sensor_msgs/PointCloud2.h"
#include "tf2_ros/buffer.h"
#include "tf2_ros/transform_listener.h"

#include <boost/filesystem.hpp>

#include <rosbag/bag.h>
#include <rosbag/view.h>

using namespace std;

namespace sunshine {

template <class WordAdapterType>
class WordExtractorNode {
    ros::Publisher words_pub, words_2d_pub;
    geometry_msgs::TransformStamped latest_transform;
    bool transform_recvd; // TODO: smarter way of handling stale/missing poses
    bool pc_recvd;
    bool use_pc;
    bool use_tf;
    bool publish_2d;
    bool publish_3d;
    bool use_latest_tf;
    double rate;
    ros::Time last_image = ros::Time(0, 0);
    std::string frame_id;
    std::string world_frame_name;
    std::string sensor_frame_name;
    std::pair<int, int> target_resolution;
    tf2_ros::Buffer tf_buffer;
    tf2_ros::TransformListener tf_listener;

    WordAdapterType wordAdapter;
    WordDepthAdapter depthAdapter;

    image_transport::ImageTransport it;
    image_transport::Subscriber imageSub;
    ros::Subscriber transformSub, odomSub;
    ros::Subscriber depthSub;

    void transformCallback(const geometry_msgs::TransformStampedConstPtr &msg) {
        // Callback to handle world to sensor transform
        if (!transform_recvd) {
            ROS_WARN("Using a dedicated transform topic is inferior to looking up tf2 transforms!");
            transform_recvd = true;
        }

        latest_transform = *msg;
        frame_id = latest_transform.header.frame_id;
    }

    void odomCallback(const nav_msgs::Odometry::ConstPtr &odometry_) {
        ROS_DEBUG("Odom callback");
        geometry_msgs::TransformStamped::Ptr tf_stamped(new geometry_msgs::TransformStamped());

        tf_stamped->header = odometry_->header;
        tf_stamped->child_frame_id = odometry_->child_frame_id;
        tf_stamped->transform.translation.x = odometry_->pose.pose.position.x;
        tf_stamped->transform.translation.y = odometry_->pose.pose.position.y;
        tf_stamped->transform.translation.z = odometry_->pose.pose.position.z;
        tf_stamped->transform.rotation = odometry_->pose.pose.orientation;

        transformCallback(tf_stamped);
    }

    void pcCallback(const sensor_msgs::PointCloud2ConstPtr &msg) {
        pcl::PointCloud<pcl::PointXYZ>::Ptr pc(new pcl::PointCloud<pcl::PointXYZ>());
        pcl::PCLPointCloud2 pcl_pc2;
        pcl_conversions::toPCL(*msg, pcl_pc2);
        pcl::fromPCLPointCloud2(pcl_pc2, *pc);

        depthAdapter.updatePointCloud(pc);
        pc_recvd = true;
    }

    void imageCallback(const sensor_msgs::ImageConstPtr &msg) {
        auto const timedelta = ros::Time::now().toSec() - this->last_image.toSec();
        if (timedelta * rate < 1.) {
            ROS_INFO_ONCE("Next frame arrived after %.2f seconds; max rate is %.2f. Dropping.", timedelta, 1. / rate);
            return;
        }
        this->last_image = ros::Time::now();

        // TODO: Add logic to handle compressed images. Refer to examples from Nathan/Vv
        cv_bridge::CvImageConstPtr img_ptr;

        if (target_resolution.first != 0 && (target_resolution.first != msg->width || target_resolution.second != msg->height)) {
            auto img = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
            cv::resize(img->image, img->image, cv::Size(target_resolution.first, target_resolution.second));
            img_ptr = img;
        } else img_ptr = cv_bridge::toCvShare(msg, sensor_msgs::image_encodings::BGR8);

        auto const imgObs = std::make_unique<ImageObservation>(sensor_frame_name, msg->header.stamp.toSec(), msg->header.seq, img_ptr->image);
        auto const wordObs = wordAdapter(imgObs.get());
        if (publish_2d) words_2d_pub.publish(toRosMsg(*wordObs));

        if (publish_3d) {
            if (use_pc && !pc_recvd) {
                ROS_ERROR("No point cloud received, observations will not be published");
                return;
            }

            auto wordObs3d = depthAdapter(wordObs.get());
            geometry_msgs::TransformStamped observation_transform;

            if (use_tf) {
                if (transform_recvd) {
                    assert(frame_id == sensor_frame_name);
                    observation_transform = latest_transform;
                } else {
                    try {
                        geometry_msgs::TransformStamped transform_msg;
                        transform_msg = tf_buffer.lookupTransform(world_frame_name,
                                                                  sensor_frame_name,
                                                                  (use_latest_tf) ? ros::Time(0) : msg->header.stamp,
                                                                  ros::Duration(0.5 / rate));
                        observation_transform = transform_msg;
                    } catch (tf2::TransformException const &ex) {
                        ROS_ERROR("No transform received: %s", ex.what());
                        //ros::Duration(1.0).sleep();
                        return;
                    }
                }
            } else {
                observation_transform = latest_transform;
                observation_transform.header = msg->header;
            }

            observation_transform.header.frame_id = sensor_frame_name;
            observation_transform.child_frame_id = world_frame_name;
            words_pub.publish(toRosMsg(*wordObs3d, observation_transform));
        }
    }

public:
    explicit WordExtractorNode(ros::NodeHandle *nh)
            : tf_listener(tf_buffer)
            , wordAdapter(nh)
            , it(*nh) {
        std::string image_topic_name, pc_topic_name, transform_topic_name, odom_topic_name;

        nh->param<string>("image", image_topic_name, "/camera/image_raw");
        nh->param<string>("transform", transform_topic_name, "");
        nh->param<string>("odom", odom_topic_name, "");
        nh->param<string>("pc", pc_topic_name, "/point_cloud");
        nh->param<bool>("use_pc", use_pc, true);
        nh->param<bool>("publish_2d_words", publish_2d, false);
        nh->param<bool>("publish_3d_words", publish_3d, true);
        nh->param<bool>("use_latest_transform", use_latest_tf, false);

        nh->param<double>("rate", rate, 4);
        string target_res;
        nh->param<string>("target_resolution", target_res, "");
        if (target_res.empty()) target_resolution = {0, 0};
        else {
            auto split = target_res.find('x');
            if (split == string::npos) throw std::invalid_argument("Invalid target resolution: " + target_res);
            target_resolution = std::make_pair(std::stoi(target_res.substr(0, split)), std::stoi(target_res.substr(split + 1)));
        }

        nh->param<bool>("use_tf", use_tf, false);
        nh->param<string>("world_frame", world_frame_name, "map");
        nh->param<string>("sensor_frame", sensor_frame_name, "base_link");

        words_pub = nh->advertise<sunshine_msgs::WordObservation>("words", 1);
        words_2d_pub = nh->advertise<sunshine_msgs::WordObservation>("words_2d", 1);

        latest_transform = {};
        latest_transform.transform.rotation.w = 1; // Default no-op rotation
        transform_recvd = false;
        pc_recvd = false;

        auto const bagfile = nh->param<string>("image_bag", "");
        if (!bagfile.empty() && boost::filesystem::exists(bagfile)) {
            ROS_WARN("Extracting images from %s", bagfile.c_str());
            ros::Rate loop_rate(rate);
            rosbag::Bag bag;
            bag.open(bagfile);

            size_t i = 0;
            for (rosbag::MessageInstance const m: rosbag::View(bag, rosbag::TopicQuery(image_topic_name))) {
                sensor_msgs::Image::ConstPtr imgMsg = m.instantiate<sensor_msgs::Image>();
                if (imgMsg != nullptr) {
                    imageCallback(imgMsg);
                } else {
                    ROS_ERROR("Non-image message found in topic %s", image_topic_name.c_str());
                }
                i++;
                if (rate > 0) loop_rate.sleep();
            }
            bag.close();
            ROS_INFO("Extracted %lu images from rosbag.", i);
        } else {
            if (!bagfile.empty()) ROS_ERROR("Rosbag image bag set but file does not exist.");
            imageSub = it.subscribe(image_topic_name, 3, &WordExtractorNode::imageCallback, this);
            if (use_tf && !transform_topic_name.empty()) {
                transformSub = nh->subscribe<geometry_msgs::TransformStamped>(transform_topic_name,
                                                                              1,
                                                                              &WordExtractorNode::transformCallback,
                                                                              this);
            }
            if (use_tf && !odom_topic_name.empty()) {
                ROS_INFO("Subscribing to odometry");
                ROS_WARN_COND(!transform_topic_name.empty(), "Subscribed to both /tf and odom_topic --- be wary of conflicts");
                odomSub = nh->subscribe<nav_msgs::Odometry>(odom_topic_name, 1, &WordExtractorNode::odomCallback, this);
            }
            if (use_pc) {
                depthSub = nh->subscribe<sensor_msgs::PointCloud2>(pc_topic_name, 1, &WordExtractorNode::pcCallback, this);
            }
        }
    }

    void spin() {
        ros::spin();
    }
};
}

#endif //SUNSHINE_PROJECT_VISUAL_WORD_EXTRACTOR_HPP
