#include "sunshine/common/data_proc_utils.hpp"
#include "sunshine/common/ros_conversions.hpp"
#include "sunshine/common/utils.hpp"
#include "sunshine/common/word_coloring.hpp"
#include <fstream>
#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <ros/ros.h>
#include <sunshine_msgs/GetTopicModel.h>
#include <sunshine_msgs/SaveObservationModel.h>
#include <sunshine_msgs/TopicMap.h>
#include <thread>
#include <fmt/printf.h>

using namespace sunshine_msgs;
using namespace cv;

int main(int argc, char** argv)
{
    ros::init(argc, argv, "visualize_topic_map");
    ros::NodeHandle nh("~");
    std::this_thread::sleep_for(std::chrono::seconds(5));
    auto const rost_namespace = nh.param<std::string>("topic_model_namespace", "rost");
    double default_pixel_scale = 1;
    try {
        nh.getParam("/" + rost_namespace + "/cell_space", default_pixel_scale);
    } catch (ros::Exception const&) {
        // do nothing
    }
    auto const pixel_scale = nh.param<double>("pixel_scale", default_pixel_scale);
    auto const maps_topic          = nh.param<std::string>("maps_topic", "/" + rost_namespace + "/topic_map");
    auto const words_topic = nh.param<std::string>("words_topic", "/" + rost_namespace + "/topics");
    auto output_prefix = nh.param<std::string>("output_prefix", "topic-map");
    auto const minWidth = nh.param<double>("min_width", 0.);
    auto const minHeight = nh.param<double>("min_height", 0.);
    auto const fixedBox = nh.param<std::string>("box", "");
    auto const useColor = nh.param<bool>("use_color", false);
    auto const continuous = nh.param<bool>("continuous", false);
    auto const saveTopicTimeseries = nh.param<bool>("save_topic_timeseries", false);
    auto const saveTopicModel = nh.param<bool>("save_topic_model", true);
    auto const saveTopicCells = nh.param<bool>("save_topic_cells", true);
    auto const saveTopicMap = nh.param<bool>("save_topic_map", true);
    auto const showTopicMap = nh.param<bool>("show_topic_map", false);
    auto const scaleTopicMap = nh.param<double>("scale_topic_map", 1.);
    auto const savePpx = nh.param<bool>("save_perplexity_map", true);
    auto const synchronized = nh.param<bool>("synchronize_with_maps", saveTopicMap);
    auto const rate = nh.param<float>("rate", 1.);
    sunshine::WordColorMap<decltype(TopicMap::cell_topics)::value_type> wordColorMap;
    ROS_INFO("Pixel scale: %f", pixel_scale);
    bool done = false;

    if (!output_prefix.empty() && *output_prefix.rbegin() != '-' && *output_prefix.rbegin() != '/') output_prefix = output_prefix + "-";

    ros::ServiceClient client = nh.serviceClient<SaveObservationModel>("/" + rost_namespace + "/save_topics_by_time_csv");
    ros::ServiceClient cellClient = nh.serviceClient<SaveObservationModel>("/" + rost_namespace + "/save_topics_by_cell_csv");
    ros::ServiceClient modelClient = nh.serviceClient<GetTopicModel>("/" + rost_namespace + "/get_topic_model");

    std::string id;
    auto const callback = [&done, &id, &wordColorMap, &client, &modelClient, &cellClient, output_prefix, pixel_scale, minWidth,
                                                          synchronized, saveTopicTimeseries, saveTopicModel, saveTopicCells, minHeight, useColor, fixedBox,
                                                          saveTopicMap, showTopicMap, scaleTopicMap](sunshine_msgs::WordObservationConstPtr const& modelMsg, sunshine_msgs::TopicMap2DConstPtr const& mapMsg) {
        assert(!mapMsg || mapMsg);
        id = (mapMsg) ? fmt::sprintf("%06d", mapMsg->topic_map.seq) : (modelMsg) ? fmt::sprintf("%10d.%09d", modelMsg->observation_transform.header.stamp.sec, modelMsg->observation_transform.header.stamp.nsec) : id;
        if (id.empty()) return;
        auto const save_extras = (!synchronized && (!mapMsg && !modelMsg)) || (synchronized && (mapMsg));

        if (save_extras && saveTopicModel) {
            GetTopicModel getTopicModel;
            if ((synchronized && mapMsg->topic_model.K != 0) || modelClient.call(getTopicModel)) {
                std::string const filename = output_prefix + id + "-modelweights.bin";
                try {
                    sunshine::CompressedFileWriter writer(filename);
                    auto model = sunshine::fromRosMsg((synchronized && mapMsg->topic_model.K != 0) ? mapMsg->topic_model : getTopicModel.response.topic_model);
                    if (!model.validate(true)) throw std::logic_error("Topic model failed validation!");
                    writer << model;
                } catch (std::logic_error const& e) {
                    ROS_ERROR("Failed to save topic model to file %s. Error: %s", filename.c_str(), e.what());
                }
            } else {
                ROS_ERROR("Failed to save topic model!");
            }
        }

        if (save_extras && saveTopicTimeseries) {
            SaveObservationModel saveObservationModel;
            saveObservationModel.request.filename = output_prefix + id + "-timeseries.csv";
            if (client.call(saveObservationModel)) {
                ROS_INFO("Saved timeseries to %s", saveObservationModel.request.filename.c_str());
            } else {
                ROS_ERROR("Failed to save topic timeseries to %s!", saveObservationModel.request.filename.c_str());
            }
        }

        if (save_extras && saveTopicCells) {
            SaveObservationModel saveObservationModel;
            saveObservationModel.request.filename = output_prefix + id + "-cells.csv";
            if (cellClient.call(saveObservationModel)) {
                ROS_INFO("Saved timeseries to %s", saveObservationModel.request.filename.c_str());
            } else {
                ROS_ERROR("Failed to save topic timeseries to %s!", saveObservationModel.request.filename.c_str());
            }
        }


        if (mapMsg && (saveTopicMap || showTopicMap)) {
            size_t const posedim = mapMsg->topic_map.cell_width.size();
            Mat topicMapImg;
            if (posedim == 3) topicMapImg = sunshine::createTopicImg<sunshine::Pose3D>(mapMsg->topic_map, wordColorMap, pixel_scale, useColor, minWidth, minHeight, fixedBox, 1, true);
            else if (posedim == 2) topicMapImg = sunshine::createTopicImg<sunshine::Pose2D>(mapMsg->topic_map, wordColorMap, pixel_scale, useColor, minWidth, minHeight, fixedBox, 1, true);
            else throw std::runtime_error("Unexpected pose dimensionality!");
            if (scaleTopicMap != 1.) cv::resize(topicMapImg, topicMapImg, cv::Size(), scaleTopicMap, scaleTopicMap, cv::INTER_NEAREST);
            if (saveTopicMap) sunshine::saveTopicImg(topicMapImg, output_prefix + std::to_string(mapMsg->topic_map.seq) + "-topics.png", output_prefix + id + "-colors.csv", &wordColorMap);
            if (showTopicMap) {
                cv::imshow(ros::this_node::getName(), topicMapImg);
                cv::waitKey(30);
            }
        }
        done = true;
    };

    auto mapSub = nh.subscribe<TopicMap2D>(maps_topic, 1, [&](sunshine_msgs::TopicMap2DConstPtr const& msg){ callback(nullptr, msg);});
    auto obsSub = nh.subscribe<WordObservation>(words_topic, 1, [&](sunshine_msgs::WordObservationConstPtr const& msg){ callback(msg, nullptr);});
    ros::Timer timer = nh.createTimer(ros::Duration(1./rate), [&](ros::TimerEvent const& e){
        if (!synchronized) callback(nullptr, nullptr);
    });

    if (continuous) {
        ros::spin();
    } else {
        ros::Rate idler(1);
        while (!done) {
            ros::spinOnce();
            idler.sleep();
        }
    }
}
