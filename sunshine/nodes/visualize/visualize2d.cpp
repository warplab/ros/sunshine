#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "sunshine_msgs/WordObservation.h"
#include "sunshine_msgs/LocalSurprise.h"
#include "rost_visualize/draw_keypoints.hpp"
#include "rost_visualize/draw_local_surprise.hpp"
#include "sunshine/common/utils.hpp"
#include <memory>
#include <filesystem>

#include <iostream>
#include <algorithm>


using namespace std;
using namespace sunshine;

static map<unsigned, std::unique_ptr<cv::Mat>> image_cache;
float scale;
static std::array<double, 3> cell_size = {0};
static int cache_size;
static std::mutex cache_lock;
static bool show_topics, show_perplexity, show_words, show_equalized;
static string image_topic_name, words_topic_name, topic_topic_name, ppx_topic_name, save_dir; //todo: rename topic model...
static double const alpha = 0.75;

static cv::Mat const& getMatchingImage(unsigned seq) {
    // cache_lock must be locked to use this!!!
    if (image_cache.find(seq) == image_cache.end()) {
        ROS_WARN("Failed to find matching image with seq number %u for topics -- using most recent image in cache, with seq %u", seq, image_cache.rbegin()->first);
        return *image_cache.rbegin()->second;
    } else {
        return *image_cache[seq];
    }
}

WordObservation msgToWordObservation(const sunshine_msgs::WordObservation::ConstPtr& z){
  WordObservation zz;
  zz.seq = z->seq;
  zz.source = z->source;
  zz.words = z->words;

  zz.word_pose.resize(z->word_pose.size());
  std::copy(std::begin(z->word_pose), std::end(z->word_pose), std::begin(zz.word_pose));
  zz.word_scale = z->word_scale;
  zz.vocabulary_begin = z->vocabulary_begin;
  zz.vocabulary_size = z->vocabulary_size;
  return zz;
}

void words_callback(const sunshine_msgs::WordObservation::ConstPtr& z){
  std::lock_guard<std::mutex> guard(cache_lock);
  if (image_cache.empty()) {
      ROS_WARN("Image cache empty! Cannot display words");
      return;
  }

  cv::Mat const& img = getMatchingImage(z->seq);

  if(img.empty()) {
      ROS_WARN("Failed to find matching image for words -- skipping");
      return;
  }
  cv::Mat img_grey;
  cv::cvtColor(img,img_grey,CV_BGR2GRAY);
  cv::Mat img_grey_3c;
  cv::cvtColor(img_grey,img_grey_3c,CV_GRAY2BGR);

  WordObservation zz = msgToWordObservation(z);

  if (zz.words.size() > 0) {
    cv::Mat out_img = draw_keypoints(zz, img_grey_3c, 5);
    cv::resize(out_img, out_img, cv::Size(), scale, scale, cv::INTER_LINEAR);
    cv::imshow("Words", out_img);
  } else {
    cv::imshow("Words", img_grey_3c);
  }
  cv::waitKey(5); 
}

cv::Mat draw_mltopic_boxes(const WordObservation& z, cv::Mat img) {
//    cv::Mat bgr_colors = get_colors(z.vocabulary_size);
    assert(z.vocabulary_size <= 16);
    cv::Mat out_img = img.clone();
    int const poseDim = z.word_pose.size() / z.words.size();
    assert(poseDim == 2);

    std::map<std::tuple<int, int>, std::vector<size_t>> hist;
    for (auto i = 0; i < z.words.size(); ++i) {
        auto key = std::make_tuple(z.word_pose[2*i], z.word_pose[2*i+1]);
        auto iter = hist.find(key);
        if (iter == hist.end()) {
            auto result = hist.emplace(std::make_pair(key, std::vector<size_t>(z.vocabulary_size, 0)));
            assert(result.second);
            iter = result.first;
        }
        assert(z.words[i] < z.vocabulary_size);
        iter->second[z.words[i]]++;
    }

    auto const cell_width = safeNumericCast<unsigned int>(cell_size[1]);
    auto const cell_height = safeNumericCast<unsigned int>(cell_size[2]);

    for (auto const& e : hist) {
        auto const label = std::max_element(e.second.begin(), e.second.end());
        if (label == e.second.end()) continue;
        int const k = label - e.second.begin();
        assert(k < 16);

        int x = std::get<0>(e.first) - cell_width / 2;
        int y = std::get<1>(e.first) - cell_width / 2;
        int width = cell_width;
        int height = cell_height;
        if (x < 0) {
            width = std::max(0, width + x);
            x = 0;
        }
        if (y < 0) {
            height = std::max(0, height + y);
            y = 0;
        }
        int max_width = img.cols - x;
        int max_height = img.rows - y;
        width = std::min(width, max_width);
        height = std::min(height, max_height);

        cv::Mat roi = out_img(cv::Rect(x, y, width, height));
        cv::Mat color(roi.size(), CV_8UC3, get_color(k));
        cv::addWeighted(color, alpha, roi, 1. - alpha, 0., roi);
    }

    return out_img;

}

void topic_callback(const sunshine_msgs::WordObservation::ConstPtr& z){
    std::lock_guard<std::mutex> guard(cache_lock);
    if (image_cache.empty()) {
        ROS_WARN("Image cache empty! Cannot display words");
        return;
    }
  cv::Mat const& img = getMatchingImage(z->seq);

  if (img.empty()) {
    ROS_INFO("Skipping empty image");
    return;
  }

  cv::Mat img_grey;
  cv::cvtColor(img,img_grey,CV_BGR2GRAY);
  cv::Mat img_grey_3c;
  cv::cvtColor(img_grey,img_grey_3c,CV_GRAY2BGR);
  
  WordObservation zz = msgToWordObservation(z);
  
  cv::Mat out_img = draw_mltopic_boxes(zz, img);
  if (!save_dir.empty()) {
      auto const base = std::filesystem::path(save_dir);
      cv::imwrite(base / (std::to_string(z->seq) + "-raw.png"), img);
      cv::imwrite(base / (std::to_string(z->seq) + "-topics.png"), out_img);
  }
  if (scale != 1) cv::resize(out_img, out_img, cv::Size(), scale, scale, cv::INTER_LINEAR);
  cv::imshow("Topics", out_img);
  cv::waitKey(5); 
}

void ppx_callback(const sunshine_msgs::LocalSurprise::ConstPtr& s_msg){
    std::lock_guard<std::mutex> guard(cache_lock);
  if (image_cache.empty()) {
      ROS_WARN("Image cache empty! Cannot display words");
      return;
  }
  // Create and normalize the image
  cv::Mat ppx_img = toMat<int32_t, double, float>(s_msg->surprise_poses, s_msg->surprise);
  cv::Scalar mean_ppx, stddev_ppx;
  cv::meanStdDev(ppx_img, mean_ppx, stddev_ppx);
  ppx_img = ppx_img - mean_ppx.val[0];
  ppx_img.convertTo(ppx_img, CV_32F, 1. / (2. * stddev_ppx.val[0]), 0.5);
  
  // Draw on the image
  cv::Mat ppx_img_color = colorize(ppx_img, cv::Vec3b(0,0,255), cv::Vec3b(255,255,255));
  
  // // Add it to the existing image
  cv::Mat const& img = getMatchingImage(s_msg->seq);

  if (img.empty()) {
    ROS_INFO("Skipping empty image");
    return;
  }
  
  cv::Mat ppx_img_full;
  cv::resize(ppx_img_color, ppx_img_full, img.size());
  cv::addWeighted(img, 0.5, ppx_img_full, 0.9, 0.0, ppx_img_full);

  if (!save_dir.empty()) {
      auto const base = std::filesystem::path(save_dir);
      cv::imwrite(base / (std::to_string(s_msg->seq) + "-ppx.png"), ppx_img_full);
  }

  cv::resize(ppx_img_full, ppx_img_full, cv::Size(), scale, scale, cv::INTER_LINEAR);
  cv::imshow("Perplexity", ppx_img_full);
  cv::waitKey(5); 
}

void image_callback(const sensor_msgs::ImageConstPtr& msg){
    std::lock_guard<std::mutex> guard(cache_lock);
  cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
  if (cv_ptr->image.empty()) {
      ROS_WARN("Empty image received! Not adding to cache");
      return;
  }

  image_cache[msg->header.seq] = std::make_unique<cv::Mat>(cv_ptr->image.clone());

  if (image_cache.size() > cache_size){
    image_cache.erase(image_cache.begin());
  }

  if (image_cache.find(msg->header.seq) == image_cache.end()) {
    ROS_ERROR("The image for seq %u wasn't actually added to the cache?!", msg->header.seq);
  } else if ( getMatchingImage(msg->header.seq).empty()) {
      ROS_ERROR("The image for seq %u is already empty?!", msg->header.seq);
  }
  
  //cv::imshow("Image", cv_ptr->image);
  //cv::waitKey(5);
}

int main(int argc, char** argv){
  ros::init(argc, argv, "viewer");
  ros::NodeHandle nhp("~");
  ros::NodeHandle nh("");

  nhp.param<float>("scale", scale, 1.0);
  nhp.param<int>("cache_size", cache_size, 100);
  nhp.param<bool>("show_topics", show_topics, true);
  nhp.param<bool>("show_words", show_words, true);
  nhp.param<bool>("show_perplexity", show_perplexity, false);
//  nhp.param<bool>("show_equalized", show_equalized, true);
  
  nhp.param<string>("words_topic", words_topic_name, "/words");
  nhp.param<string>("topics_topic", topic_topic_name, "/topics");
  nhp.param<string>("ppx_topic", ppx_topic_name, "/cell_perplexity");
  nhp.param<string>("image", image_topic_name, "/camera/image_raw");
  nhp.param<string>("save_dir", save_dir, "");

  if (!save_dir.empty()) {
      int const box_size = 50;
      int const n_colors = 16;
      cv::Mat colorPalette(box_size * n_colors, box_size, CV_8UC3, cv::Scalar(0, 0, 0));
      for (auto k = 0; k < n_colors; ++k) {
          cv::Mat roi = colorPalette(cv::Rect(0, k * box_size, box_size, box_size));
          cv::Mat color(roi.size(), CV_8UC3, get_color(k));
          cv::addWeighted(color, alpha, roi, 1. - alpha, 0., roi);
      }
      cv::imwrite(std::filesystem::path(save_dir) / "topics-palette.png", colorPalette);
  }

  auto const cell_size_string = nhp.param<std::string>("cell_size", "");
  auto const cell_size_space = nhp.param<double>("cell_space", 1);
  auto const cell_size_time = nhp.param<double>("cell_time", 1);
  if (!cell_size_string.empty()) {
      cell_size = readNumbers<3, 'x'>(cell_size_string);
  } else {
      cell_size = computeCellSize<3>(cell_size_time, cell_size_space);
  }

  image_transport::ImageTransport it(nhp);
  image_transport::Subscriber img_sub = it.subscribe(image_topic_name, 1, image_callback);

  /*
  ros::Subscriber word_sub = nhp.subscribe(words_topic_name,1,words_callback);
  ros::Subscriber topic_sub = nhp.subscribe(topic_topic_name,1,topic_callback);
  ros::Subscriber ppx_sub = nhp.subscribe(ppx_topic_name,1,ppx_callback);
  */

  ros::Subscriber word_sub, topic_sub, ppx_sub;

  if (show_topics) topic_sub = nhp.subscribe(topic_topic_name,1,topic_callback);
  if (show_words) word_sub = nhp.subscribe(words_topic_name,1,words_callback);
  if (show_perplexity) ppx_sub = nhp.subscribe(ppx_topic_name,1,ppx_callback);
  
  ros::spin();

  return 0;
}
