//
// Created by stewart on 3/2/23.
//

#ifndef SUNSHINE_PROJECT_SERIALIZATION_UTILS_H
#define SUNSHINE_PROJECT_SERIALIZATION_UTILS_H

#include "sunshine_msgs/TopicMap.h"
#include "sunshine/rost_adapter.hpp"
#include "csv.hpp"

namespace sunshine {
    using namespace sunshine_msgs;

    template<typename T = sunshine::ROSTAdapter<3>>
    void save_topics_csv(T const &rost, std::string const &filename, bool const &normalize_cell_counts) {
        std::ofstream writer(filename);
        writer << "pose_dim_0";
        static constexpr auto PoseDim = std::remove_reference_t<decltype(rost)>::PoseDim;
        for (auto i = 1; i < PoseDim; i++) { writer << ",pose_dim_" + std::to_string(i); }
        for (auto k = 0; k < rost.get_num_topics(); k++) { writer << ",k_" << std::to_string(k); }
        if (normalize_cell_counts) {
            auto const topics = rost.template get_topics_by_cell<true>();
            for (auto const &entry : topics) {
                writer << "\n";
                writer << std::to_string(entry.first[0]);
                for (auto dim = 1u; dim < PoseDim; dim++) { writer << "," + std::to_string(entry.first[dim]); }
                for (auto const &count : entry.second) { writer << "," + std::to_string(count); }
            }
        } else {
            auto const topics = rost.template get_topics_by_cell<false>();
            for (auto const &entry : topics) {
                writer << "\n";
                writer << std::to_string(entry.first[0]);
                for (auto dim = 1u; dim < PoseDim; dim++) { writer << "," + std::to_string(entry.first[dim]); }
                for (auto const &count : entry.second) { writer << "," + std::to_string(count); }
            }
        }
        writer.close();
    }

    template<typename T = sunshine::ROSTAdapter<3>>
    TopicMapPtr generate_topic_map(T const &rostAdapter, int const obs_time, bool const fullDist, bool const trim_unused, int const min_words_per_map_cell = 0) {
        auto const& rost = rostAdapter.get_rost();
        static constexpr auto PoseDim = std::remove_reference_t<decltype(rostAdapter)>::PoseDim;
        auto const &topic_weights = rost.get_topic_weights();
        int32_t K                 = rost.get_num_topics();
        if (trim_unused)
            for (; K > 0 && topic_weights[K - 1] == 0; --K)
                ;
        TopicMap::Ptr topic_map = boost::make_shared<TopicMap>();
        topic_map->seq          = static_cast<uint32_t>(obs_time);
        //    topic_map->header.frame_id  = rostAdapter.get_world_frame();
        topic_map->vocabulary_begin = 0;
        topic_map->vocabulary_size  = K;
        topic_map->ppx_type         = "none";
        topic_map->cell_topics.reserve(rost.cell_pose.size() * ((fullDist) ? K : 1));
        topic_map->cell_poses.reserve(rost.cell_pose.size() * (PoseDim - 1));
        topic_map->cell_width                                 = {rostAdapter.get_cell_size().begin() + 1, rostAdapter.get_cell_size().end()};
        topic_map->observation_transform.transform.rotation.w = 1; // Identity rotation (global frame)
        topic_map->observation_transform.header.stamp         = ros::Time::now();

        auto const poses      = rost.cell_pose;
        auto const BATCH_SIZE = 500ul;
        auto i                = 0ul;
        while (i < poses.size()) {
            auto const BATCH_END = std::min(i + BATCH_SIZE, poses.size());
            for (; i < BATCH_END; ++i) {
                auto const &cell_pose  = poses[i];
                auto const &cell       = rost.get_cell(cell_pose);
                auto const cell_weight = std::accumulate(cell->nZ.cbegin(), cell->nZ.cend(), 0);
                if (cell_weight < min_words_per_map_cell) {
                    ROS_WARN_COND(cell_weight == 0, "Cell has no topics, skipping");
                    continue;
                }
                if (fullDist) {
                    topic_map->cell_topics.insert(topic_map->cell_topics.end(), cell->nZ.cbegin(), cell->nZ.cbegin() + K);
                } else {
                    auto const ml_cell_topic = std::max_element(cell->nZ.cbegin(), cell->nZ.cend());
                    ROS_ERROR_COND(ml_cell_topic == cell->nZ.cend(), "Failed to find maximum likelihood topic?!");
                    topic_map->cell_topics.push_back(int32_t(ml_cell_topic - cell->nZ.cbegin()));
                }
                for (size_t j = 1; j < PoseDim; j++) { topic_map->cell_poses.push_back(cell_pose[j]); }
            }
        }
        return topic_map;
    }
}

#endif // SUNSHINE_PROJECT_SERIALIZATION_UTILS_H
