//
// Created by stewart on 9/7/20.
//

#ifndef SUNSHINE_PROJECT_DATA_PROC_UTILS_HPP
#define SUNSHINE_PROJECT_DATA_PROC_UTILS_HPP

#include "utils.hpp"
#include "word_coloring.hpp"
#include <fstream>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <ros/console.h>
#include <sunshine_msgs/TopicMap.h>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/iostreams/filter/zlib.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <fstream>
#include <sunshine_msgs/DenseTopicMap.h>
#include <utility>

namespace sunshine {

class CompressedFileWriter {
    std::ofstream file;
    std::unique_ptr<boost::iostreams::filtering_ostream> filtered_out;
    boost::archive::text_oarchive output_stream;

    static std::unique_ptr<boost::iostreams::filtering_ostream> configureStream(std::ofstream &file) {
        auto filtered_out = std::make_unique<boost::iostreams::filtering_ostream>();
        filtered_out->set_auto_close(true);
        filtered_out->push(boost::iostreams::zlib_compressor());
        filtered_out->push(file);
        return filtered_out;
    }

  public:
    explicit CompressedFileWriter(std::string const &filename)
        : file(filename, std::ios_base::out | std::ios_base::binary)
        , filtered_out(configureStream(file))
        , output_stream(*filtered_out) { }

    void flush() {
        filtered_out->flush();
    }

    template<typename DataType>
    void operator<<(DataType const &data) {
        output_stream &data;
        flush();
    }
};

class CompressedFileReader {
    std::ifstream file;
    std::unique_ptr<boost::iostreams::filtering_istream> filtered_in;
    boost::archive::text_iarchive input_stream;

    static std::unique_ptr<boost::iostreams::filtering_istream> configureStream(std::ifstream &file) {
        auto filtered_in = std::make_unique<boost::iostreams::filtering_istream>();
        filtered_in->set_auto_close(true);
        filtered_in->push(boost::iostreams::zlib_decompressor());
        filtered_in->push(file);
        return filtered_in;
    }

  public:
    explicit CompressedFileReader(std::string const &filename)
        : file(filename, std::ios_base::in | std::ios_base::binary)
        , filtered_in(configureStream(file))
        , input_stream(*filtered_in) { }

    template<typename DataType>
    void operator>>(DataType &data) {
        input_stream &data;
    }

    template<typename DataType>
    DataType read() {
        DataType data;
        *this >> data;
        return std::move(data);
    }

    bool eof() const {
        return file.eof();
    }

    operator bool() const {
        return file.good();
    }
};

struct Pose2D {
    int x, y;
};

struct Pose3D {
    int x, y, z;
};

template<class PoseT = Pose3D>
cv::Mat createTopicImg(const sunshine_msgs::TopicMap &msg,
                       sunshine::WordColorMap<decltype(sunshine_msgs::TopicMap::cell_topics)::value_type> &wordColorMap,
                       double const pixel_scale,
                       bool const useColor,
                       double minWidth             = 0,
                       double minHeight            = 0,
                       const std::string &fixedBox = "",
                       uint32_t const upsample     = 1,
                       bool include_endpoints      = true) {
    using namespace sunshine_msgs;
    using namespace cv;
    size_t const K = msg.vocabulary_size;
    size_t const D = msg.cell_width.size();
    size_t const N = msg.cell_poses.size() / D;
    bool const mlTopics = (msg.cell_topics.size() == N && K > 1) || (K == 1 && std::all_of(msg.cell_topics.cbegin(), msg.cell_topics.cend(), [&msg](int32_t const& v){
                                                                         return v == msg.vocabulary_begin;
                                                                     }));
    bool const fullDist = !mlTopics && msg.cell_topics.size() == N * K;
    if (!mlTopics && !fullDist) throw std::runtime_error("Unable to interpret contents of cell_topics");
    auto const x_size = msg.cell_width[0];
    auto const y_size = msg.cell_width[1];

    if (sizeof(PoseT) / sizeof(*msg.cell_poses.data()) != D) throw std::logic_error("Pose type has incorrect size/dimensionality!");
//    static_assert(sizeof(Pose) == sizeof(*msg.cell_poses.data()) * 3, "Pose struct has incorrect size.");

    PoseT const *poseIter = reinterpret_cast<PoseT const *>(msg.cell_poses.data());
    double minX, minY, maxX, maxY;
    if (fixedBox.empty()) {
        minX = poseIter->x;
        minY = poseIter->y;
        maxX = poseIter->x;
        maxY = poseIter->y;
        for (size_t i = 1; i < N; i++, poseIter++) {
            minX = std::min(poseIter->x, (int) minX);
            maxX = std::max(poseIter->x, (int) maxX);
            minY = std::min(poseIter->y, (int) minY);
            maxY = std::max(poseIter->y, (int) maxY);
        }
        minX *= x_size;
        minY *= y_size;
        maxX *= x_size;
        maxY *= y_size;

        maxX = std::max(maxX, minX + minWidth);
        maxY = std::max(maxY, minY + minHeight);
    } else {
        auto const size_spec = sunshine::readNumbers<4, 'x'>(fixedBox);
        minX                 = size_spec[0];
        minY                 = size_spec[1];
        maxX                 = size_spec[0] + size_spec[2];
        maxY                 = size_spec[1] + size_spec[3];
    }

    ROS_DEBUG("Generating map over region (%f, %f) to (%f, %f) (size spec %s)", minX, minY, maxX, maxY, fixedBox.c_str());

    uint32_t const numRows = std::floor((maxY - minY) / pixel_scale) + include_endpoints;
    uint32_t const numCols = std::floor((maxX - minX) / pixel_scale) + include_endpoints;

    Mat topicMapImg(numRows, numCols, (useColor) ? sunshine::cvType<Vec4b>::value : sunshine::cvType<double>::value, Scalar(0));
    std::set<std::pair<int, int>> points;
    poseIter        = reinterpret_cast<PoseT const *>(msg.cell_poses.data());
    size_t outliers = 0, overlaps = 0;
    for (size_t i = 0; i < N; i++, poseIter++) {
        Point const point(std::round((poseIter->x * x_size - minX) / pixel_scale), std::round((maxY - poseIter->y * y_size) / pixel_scale));
        if (point.x < 0 || point.y < 0 || point.x >= numRows || point.y >= numCols) {
            outliers++;
            continue;
        }
        if (!points.insert({point.x, point.y}).second) {
            ROS_INFO_THROTTLE(1, "Duplicate cells found at (%d, %d)", point.x, point.y);
            overlaps++;
        }
        int32_t mlTopic;
        if (mlTopics) mlTopic = msg.cell_topics[i];
        else {
            assert(fullDist);
            auto const start = msg.cell_topics.cbegin() + i * K;
            auto const end = start + K;
            auto const& itmax = std::max_element(start, end);
            if (*itmax == 0) continue;
            mlTopic = int32_t(itmax - start);
        }
        assert(mlTopic >= 0 && mlTopic < K);
        if (useColor) {
            auto const color             = wordColorMap.colorForWord(mlTopic);
            topicMapImg.at<Vec4b>(point) = {color.r, color.g, color.b, color.a};
        } else {
            topicMapImg.at<double>(point) = mlTopic + 1;
        }
    }
    ROS_DEBUG_COND(outliers > 0, "Discarded %lu points outside of %s", outliers, fixedBox.c_str());
    ROS_DEBUG_COND(overlaps > 0, "Discarded %lu overlapped points.", overlaps);
    ROS_DEBUG("Points: %lu, Rows: %d, Cols: %d, Colors: %lu", N, numRows, numCols, wordColorMap.getNumColors());
    if (static_cast<unsigned long>(numRows) * static_cast<unsigned long>(numCols) < N) {
        ROS_INFO("More cells than space in grid - assuming there are overlapping cells.");
    } else {
        ROS_DEBUG("Gaps: %lu", static_cast<unsigned long>(numRows) * static_cast<unsigned long>(numCols) - N);
    }
    if (upsample > 1) {
        Mat scaledImage;
        cv::resize(topicMapImg, scaledImage, Size(0, 0), upsample, upsample, cv::INTER_NEAREST);
        return scaledImage;
    }
    return topicMapImg;
}

cv::Mat createTopicImg(const sunshine_msgs::DenseTopicMap &msg,
                       sunshine::WordColorMap<decltype(sunshine_msgs::TopicMap::cell_topics)::value_type> &wordColorMap,
                       bool const useColor,
                       uint32_t const upsample     = 1) {
    using namespace sunshine_msgs;
    using namespace cv;
    int32_t const numRows = msg.info.height;
    int32_t const numCols = msg.info.width;
    auto const K = msg.K;
    assert(numRows * numCols * K == msg.map_topic_proportions.size());

    Mat topicMapImg(numRows, numCols, (useColor) ? sunshine::cvType<Vec4b>::value : sunshine::cvType<float>::value, Scalar(0));
    for (int x = 0; x < numCols; ++x) {
        for (int y = 0; y < numRows; ++y) {
            cv::Point const pt(x, (numRows - 1) - y);
            auto const start = msg.map_topic_proportions.begin() + (x * numRows + y) * K;
            auto const end = start + K;
            auto const& itmax = std::max_element(start, end);
            if (*itmax == 0) continue;
            auto const mlTopic = int32_t(itmax - start);

            assert(mlTopic >= 0 && mlTopic < K);
            if (useColor) {
                auto const color             = wordColorMap.colorForWord(mlTopic);
                topicMapImg.at<Vec4b>(pt) = {color.r, color.g, color.b, color.a};
            } else {
                topicMapImg.at<float>(pt) = mlTopic + 1;
            }
        }
    }
    ROS_INFO("Rows: %d, Cols: %d, Colors: %lu", numRows, numCols, wordColorMap.getNumColors());
    if (upsample > 1) {
        Mat scaledImage;
        cv::resize(topicMapImg, scaledImage, Size(0, 0), upsample, upsample, cv::INTER_NEAREST);
        return scaledImage;
    }
    return topicMapImg;
}

template<typename WordColorMapType = WordColorMap<int>>
void saveTopicImg(cv::Mat const &topicMapImg,
                  const std::string &filename,
                  const std::string &colors_filename = "",
                  WordColorMapType *colorMap         = nullptr) {
    cv::imwrite(filename, topicMapImg);
    if (!colors_filename.empty()) {
        if (colorMap == nullptr) throw std::invalid_argument("Must provide color map to save colors");
        std::ofstream colorWriter(colors_filename);
        for (auto const &entry : colorMap->getAllColors()) {
            colorWriter << entry.first;
            for (auto const &v : entry.second) { colorWriter << "," << std::to_string(v); }
            colorWriter << "\n";
        }
        colorWriter.close();
    }
}

} // namespace sunshine

#endif // SUNSHINE_PROJECT_DATA_PROC_UTILS_HPP
