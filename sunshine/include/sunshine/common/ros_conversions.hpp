//
// Created by stewart on 3/4/20.
//

#ifndef SUNSHINE_PROJECT_ROS_CONVERSIONS_HPP
#define SUNSHINE_PROJECT_ROS_CONVERSIONS_HPP

#include "sunshine/common/observation_types.hpp"
#include "sunshine/common/sunshine_types.hpp"
#include "sunshine/common/utils.hpp"
#include <cv_bridge/cv_bridge.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/TransformStamped.h>
#include <ros/console.h>
#include <sensor_msgs/Image.h>
#include <sunshine_msgs/TopicMap.h>
#include <sunshine_msgs/TopicModel.h>
#include <sunshine_msgs/WordObservation.h>
#include <tf2/convert.h>

#include <sunshine_msgs/TopicMap2D.h>
#include <utility>

namespace sunshine {

template<typename WordType, uint32_t PoseDim, typename PoseType = double>
sunshine_msgs::WordObservation toRosMsg(CategoricalObservation<WordType, PoseDim, PoseType> const& in,
                                        geometry_msgs::TransformStamped observationTransform) {
    static_assert(std::is_integral<WordType>::value, "Only integral word types are supported!");
    static_assert(sizeof(WordType) <= sizeof(decltype(sunshine_msgs::WordObservation::words)::value_type), "WordType is too large!");
    auto constexpr poseDim = PoseDim;

    sunshine_msgs::WordObservation out{};
    out.header.frame_id = in.frame;
    //    out.header.seq = in.id; // deprecate?
    out.header.stamp          = ros::Time(in.timestamp);
    out.seq                   = in.id;
    out.source                = ""; // unused
    out.vocabulary_begin      = in.vocabulary_start;
    out.vocabulary_size       = in.vocabulary_size;
    out.observation_transform = std::move(observationTransform);

    out.words.reserve(in.observations.size());
    out.word_pose.reserve(in.observations.size() * poseDim);
    out.word_scale = {}; // uninitialized

    for (auto i = 0ul, j = 0ul; i < in.observations.size(); ++i) {
        out.words.emplace_back(in.observations[i]);
        assert(in.observation_poses[i].size() == poseDim);
        for (auto d = 0u; d < poseDim; ++d) { out.word_pose.emplace_back(static_cast<double>(in.observation_poses[i][d])); }
    }
    return out;
}

template<typename WordType, uint32_t PoseDim, typename PoseType = double>
sunshine_msgs::WordObservation toRosMsg(CategoricalObservation<WordType, PoseDim, PoseType> const& in) {
    static_assert(std::is_integral<WordType>::value, "Only integral word types are supported!");
    static_assert(sizeof(WordType) <= sizeof(decltype(sunshine_msgs::WordObservation::words)::value_type), "WordType is too large!");
    auto constexpr poseDim = PoseDim;

    sunshine_msgs::WordObservation out{};
    out.header.frame_id = in.frame;
    //    out.header.seq = in.id; // deprecate?
    out.header.stamp                               = ros::Time(in.timestamp);
    out.seq                                        = in.id;
    out.source                                     = ""; // unused
    out.vocabulary_begin                           = in.vocabulary_start;
    out.vocabulary_size                            = in.vocabulary_size;
    out.observation_transform                      = {};
    out.observation_transform.transform.rotation.w = 1;

    out.words.reserve(in.observations.size());
    out.word_pose.reserve(in.observations.size() * poseDim);
    out.word_scale = {}; // uninitialized

    for (auto i = 0ul; i < in.observations.size(); ++i) {
        out.words.emplace_back(in.observations[i]);
        assert(in.observation_poses[i].size() == poseDim);
        for (auto d = 0u; d < poseDim; ++d) { out.word_pose.emplace_back(static_cast<double>(in.observation_poses[i][d])); }
    }
    return out;
}

template<typename WordType, uint32_t PoseDim, typename WordPoseType>
CategoricalObservation<WordType, PoseDim, WordPoseType> fromRosMsg(sunshine_msgs::WordObservation const& in) {
    static_assert(PoseDim <= 4, "Only up to 4-d poses are currently supported for ROS conversion");
    static_assert(std::is_integral<WordType>::value, "Only integral word types are supported!");
    static_assert(sizeof(WordType) <= sizeof(decltype(sunshine_msgs::WordObservation::words)::value_type), "WordType is too large!");

    auto out = CategoricalObservation<WordType, PoseDim, WordPoseType>(
        (in.observation_transform.child_frame_id.empty()) ? in.header.frame_id : in.observation_transform.child_frame_id,
        in.header.stamp.toSec(), in.seq, {}, {}, in.vocabulary_begin, in.vocabulary_size);

    out.observations.reserve(in.words.size());
    out.observation_poses.reserve(in.words.size());

    uint32_t const inputDim = (in.words.empty()) ? PoseDim : in.word_pose.size() / in.words.size();
    if (inputDim != PoseDim) throw std::logic_error("PoseDim does not match pose dimension of input message!");
    if (in.words.size() * inputDim != in.word_pose.size()) { throw std::invalid_argument("Malformed sunshine_msgs::WordObservation"); }

    for (auto i = 0ul, j = 0ul; i < in.words.size(); ++i, j += inputDim) {
        out.observations.emplace_back(in.words[i]);
        assert(j + PoseDim <= in.word_pose.size());

        geometry_msgs::Point point;
        switch (inputDim) {
        case 3:
            point.z = in.word_pose[j + 2];
        case 2:
            point.y = in.word_pose[j + 1];
        case 1:
            point.x = in.word_pose[j];
            break;
        default:
            throw std::logic_error("Cannot handle more than 3 input dimensions");
        }
        std::array<WordPoseType, PoseDim> wordPose = {0};
        tf2::doTransform(point, point, in.observation_transform);
        switch (inputDim) {
        case 3:
            wordPose[2] = point.z;
        case 2:
            wordPose[1] = point.y;
        case 1:
            wordPose[0] = point.x;
            break;
        default:
            throw std::logic_error("Cannot handle more than 3 input dimensions");
        }
        out.observation_poses.push_back(wordPose);
    }
    return out;
}

ImageObservation fromRosMsg(const sensor_msgs::ImageConstPtr& msg) {
    cv_bridge::CvImagePtr img_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    return ImageObservation(msg->header.frame_id, msg->header.stamp.toSec(), msg->header.seq, std::move(img_ptr->image));
}

Phi fromRosMsg(sunshine_msgs::TopicModel const& topic_model) {
    Phi out(topic_model.identifier, topic_model.K, topic_model.V, {}, topic_model.topic_weights);
    assert(topic_model.topic_weights.empty() || *std::min_element(topic_model.topic_weights.cbegin(), topic_model.topic_weights.cend()) >= 0);
    out.counts.reserve(topic_model.K);
    for (auto i = 0ul; i < topic_model.K; ++i) {
        out.counts.emplace_back(topic_model.phi.begin() + i * topic_model.V,
                                (i + 1 < topic_model.K) ? topic_model.phi.begin() + (i + 1) * topic_model.V : topic_model.phi.end());
        assert(out.counts[i].size() == topic_model.V);
    }
    return out;
}

sunshine_msgs::TopicModel toRosMsg(Phi const& phi, bool const& trim_unused = false) {
    sunshine_msgs::TopicModel topicModel;
    topicModel.K = phi.K;
    if (trim_unused) while (topicModel.K > 0 && phi.topic_weights[topicModel.K - 1] == 0) --topicModel.K;
    topicModel.V             = phi.V;
    topicModel.identifier    = phi.id;
    topicModel.topic_weights = {phi.topic_weights.begin(), phi.topic_weights.begin() + topicModel.K};
    topicModel.phi.reserve(topicModel.K * phi.V);
    for (auto i = 0ul; i < topicModel.K; ++i) {
        auto topicDist = (std::vector<int>) phi.counts[i];
        topicModel.phi.insert(topicModel.phi.end(), topicDist.begin(), topicDist.end());
    }
    assert(topicModel.phi.size() == topicModel.K * topicModel.V);
    return topicModel;
}

template<typename LabelType, uint32_t PoseDim>
sunshine_msgs::TopicMap2D toRosMsg(Segmentation<LabelType, PoseDim, int, double> const& segmentation, bool fullDist = false) {
    sunshine_msgs::TopicMap2D map2d;
    map2d.header.frame_id = segmentation.frame;
    map2d.header.stamp    = map2d.header.stamp.fromSec(segmentation.timestamp);
    sunshine_msgs::TopicMap& map = map2d.topic_map;
    map.seq             = segmentation.id;
    map.vocabulary_begin = 0;
    if constexpr (std::is_same_v<LabelType, int>) map.vocabulary_size = *std::max_element(segmentation.observations.cbegin(), segmentation.observations.cend()) + 1;
    else if constexpr (std::is_same_v<LabelType, std::vector<int>>) map.vocabulary_size = (segmentation.observations.empty()) ? 0 : segmentation.observations[0].size();
    // TODO: we should require a pose descriptor/specifier e.g. 'xyz', 'txy', 'txyz', etc.
    // For now we assume 'x', 'xy', 'txy', or 'txyz'
    static_assert(PoseDim > 0 && PoseDim <= 4, "Unsupported pose dimensionality");
    map.cell_time             = (PoseDim >= 3) ? segmentation.cell_size[0] : -1;
    constexpr uint32_t offset = (PoseDim >= 3);
    map.cell_width            = {segmentation.cell_size[offset], segmentation.cell_size[offset + 1]};
    map.cell_poses.reserve(segmentation.observation_poses.size() * 2);
    auto const cell_size = segmentation.cell_size;
    std::for_each(segmentation.observation_poses.begin(), segmentation.observation_poses.end(),
                  [&map, offset, &cell_size](std::array<int, PoseDim> const& pose) {
                      map.cell_poses.insert(map.cell_poses.end(), pose.begin() + offset, pose.begin() + (offset + 2));
                  });
    if constexpr (std::is_same_v<LabelType, int>) {
        if (fullDist) throw std::logic_error("Cannot produce full topic distributions from a maximum-likelihood segmentation");
        map.cell_topics = segmentation.observations;
    } else if constexpr (std::is_same_v<LabelType, std::vector<int>>) {
        if (fullDist) {
            auto const K = (segmentation.observations.empty()) ? 0 : segmentation.observations[0].size();
            map.cell_topics.reserve(segmentation.observations.size() * K);
            for (auto const& obs : segmentation.observations) {
//                if (std::accumulate(obs.begin(), obs.end(), 0.f) == 0.) throw std::invalid_argument("Invalid segmentation, some observations are empty");
                for (auto const& z : obs) {
                    map.cell_topics.push_back(z);
                }
            }
            if (map.cell_topics.size() != segmentation.observations.size() * K || map.cell_poses.size() != segmentation.observations.size() * 2) {
                throw std::logic_error("Invalid cell_topics or cell_poses size!");
            }
        } else {
            map.cell_topics.reserve(segmentation.observations.size());
            std::transform(segmentation.observations.begin(), segmentation.observations.end(), std::back_inserter(map.cell_topics),
                           argmax<std::vector<int>>);
            if (map.cell_topics.size() != segmentation.observations.size() || map.cell_poses.size() != segmentation.observations.size() * 2) {
                throw std::logic_error("Invalid cell_topics or cell_poses size!");
            }
        }
    } else {
        static_assert(always_false<LabelType>);
    }
    return map2d;
}

template<typename LabelType>
Segmentation<LabelType, 2, int, double> fromRosMsg(sunshine_msgs::TopicMap2D const& map2d) {
    Segmentation<LabelType, 2, int, double> segmentation;
    auto const& map        = map2d.topic_map;
    segmentation.frame     = map2d.header.frame_id;
    segmentation.timestamp = map2d.header.stamp.toSec();
    segmentation.id        = map.seq;
    segmentation.cell_size = {map.cell_width[0], map.cell_width[1]};
    auto const K           = map.vocabulary_size;
    if (K <= 0) {
        ROS_ERROR_COND(K < 0, "K must be nonnegative!");
        assert(map.cell_topics.empty() && map.cell_poses.empty());
        return segmentation;
    } else if (map.cell_topics.empty()) {
        assert(map.cell_poses.empty());
        return segmentation;
    } else assert(!map.cell_poses.empty());
    auto const D = map.cell_width.size();
    auto const N = map.cell_poses.size() / D;
    if (N * D != map.cell_poses.size()) throw std::invalid_argument("Cell width dimensionality does not match cell pose dimensionality!");
    bool const mlLabels = (map.cell_topics.size() == N && K > 1) || (K == 1 && std::all_of(map.cell_topics.cbegin(), map.cell_topics.cend(), [&map](int32_t const& v){
                              return v == map.vocabulary_begin;
                          }));
    bool const fullDist = !mlLabels && (map.cell_topics.size() == N * K);
    if (!mlLabels && !fullDist) throw std::invalid_argument("Unable to infer meaning of cell_topics!");
    segmentation.observation_poses.reserve(N);
    for (auto i = 0; i < map.cell_poses.size(); i += D) {
        segmentation.observation_poses.push_back(std::array<int, 2>{map.cell_poses[i], map.cell_poses[i + 1]});
    }
    if constexpr (std::is_same_v<LabelType, int>) {
        if (mlLabels) segmentation.observations = map.cell_topics;
        else {
            assert(fullDist);
            ROS_INFO("Computing maximum likelihood topics from full distribution");
            for (auto i = 0; i < map.cell_topics.size(); i += K) {
                auto start = map.cell_topics.cbegin() + i;
                auto end = start + K;
                segmentation.observations.push_back(int32_t(std::max_element(start, end) - start));
                assert(segmentation.observations.back() >= 0 && segmentation.observations.back() < K);
            }
        }
    } else if constexpr (std::is_same_v<LabelType, std::vector<int>>) {
        if (!fullDist)
            throw std::runtime_error("Cannot generate topic distributions given only maximum likelihood labels");
        for (long i = 0; i < map.cell_topics.size(); i += K) {
            segmentation.observations.emplace_back(map.cell_topics.cbegin() + i, map.cell_topics.cbegin() + i + K);
        }
    } else {
        static_assert(always_false<LabelType>);
    }
    return segmentation;
}

} // namespace sunshine

#endif // SUNSHINE_PROJECT_ROS_CONVERSIONS_HPP
