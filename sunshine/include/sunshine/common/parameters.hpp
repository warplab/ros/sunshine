//
// Created by stewart on 3/24/20.
//

#ifndef SUNSHINE_PROJECT_PARAMETERS_HPP
#define SUNSHINE_PROJECT_PARAMETERS_HPP

#include <string>
#include <map>
#include <utility>
#include <variant>
#include "sunshine/external/json.hpp"
#include <mutex>

namespace sunshine {

class Parameters {
    mutable std::mutex lock;
    mutable std::map<std::string, std::variant<std::string, int, double, bool>> parameters;
    bool const saveDefaults;
  public:
    explicit Parameters(decltype(parameters) params, bool saveDefaults = true)
          : parameters(std::move(params))
          , saveDefaults(saveDefaults) {}

    Parameters(Parameters const& other)
        : saveDefaults(other.saveDefaults) {
        std::lock_guard<std::mutex> guard(other.lock);
        this->parameters = other.parameters;
    }

    std::variant<std::string, int, double, bool>& operator[](std::string const& key) const {
        std::lock_guard<std::mutex> guard(lock);
        return parameters[key];
    }

    template<typename T>
    T const& get(std::string const& key) const {
        std::lock_guard<std::mutex> guard(lock);
        return std::get<T>(parameters[key]);
    }

    template<typename T>
    T param(std::string const &key, T default_value) {
        std::lock_guard<std::mutex> guard(lock);
        if (auto const &iter = parameters.find(key); iter != parameters.end()) {
            try {
                return std::get<T>(iter->second);
            } catch (std::bad_variant_access const& e) {
                throw std::runtime_error(("Invalid type for parameter " + key).c_str());
            }
        } else {
            if (saveDefaults) parameters.insert({key, default_value});
            return default_value;
        }
    }

    bool has(std::string const& key) const {
        std::lock_guard<std::mutex> guard(lock);
        return parameters.find(key) != parameters.end();
    }

    nlohmann::json toJson() const {
        std::lock_guard<std::mutex> guard(lock);
        nlohmann::json obj;
        for (auto const& e : parameters) {
            auto const& key = e.first;
            std::visit([&obj, key](auto const& v){
                obj[key] = v;
            }, e.second);
        }
        return obj;
    }

    static Parameters fromJson(nlohmann::json const& obj, bool saveDefaults) {
        decltype(parameters) params;
        for (auto const& e : obj.items()) {
            switch (e.value().type()) {
            case nlohmann::detail::value_t::number_integer:
                params.insert({e.key(), e.value().get<int>()});
                break;
            case nlohmann::detail::value_t::number_float:
                params.insert({e.key(), e.value().get<double>()});
                break;
            case nlohmann::detail::value_t::boolean:
                params.insert({e.key(), e.value().get<bool>()});
                break;
            case nlohmann::detail::value_t::string:
                params.insert({e.key(), e.value().get<std::string>()});
                break;
            default:
                throw std::invalid_argument("Unknown json object type");
            }
        }
        return Parameters(params, saveDefaults);
    }
};

}

#endif //SUNSHINE_PROJECT_PARAMETERS_HPP
