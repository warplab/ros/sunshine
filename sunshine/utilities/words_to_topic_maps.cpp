//
// Created by stewart on 1/11/23.
//

#include "sunshine/common/parameters.hpp"
#include "sunshine/external/json.hpp"
#include "sunshine/rost_adapter.hpp"
#include "sunshine/common/serialization_utils.h"
#include <boost/progress.hpp>
#include "sunshine/common/data_proc_utils.hpp"
#include "opencv2/highgui.hpp"
#include <fmt/printf.h>

using namespace sunshine;
using namespace std::chrono;
using json = nlohmann::ordered_json;

int main(int argc, char** argv) {
    if (argc < 4) {
        std::cerr << "Usage: ./utils.words_to_topic_maps words.json count output_prefix [K alpha beta gamma cell_space cell_time]"
                  << std::endl;
        return 1;
    }
    std::string const words_path(argv[1]);
    int const count = std::stoi(argv[2]);
    std::string output_prefix(argv[3]);
    int K= 10;
    double alpha = 0.06, beta = 0.4, gamma = 0.00000139, cell_space = 0.5, cell_time=36000;
    if (argc >= 5) K = std::stoi(argv[4]);
    if (argc >= 6) alpha = std::stod(argv[5]);
    if (argc >= 7) beta = std::stod(argv[6]);
    if (argc >= 8) gamma = std::stod(argv[7]);
    if (argc >= 9) cell_space = std::stod(argv[8]);
    if (argc >= 10) cell_time = std::stod(argv[9]);

    output_prefix += "K={:d}_a={:f}_b={:f}_g={:f}_cellspace={:g}_";
    output_prefix = fmt::format(output_prefix, K, alpha, beta, gamma, cell_space);
//    sunshine::Parameters parameters({}, true);
//    if (argc >= 4) {
//        std::string const params_path(argv[4]);
//        std::ifstream params_stream(params_path);
//        parameters = sunshine::Parameters::fromJson(nlohmann::json::parse(params_stream), true);
//    } else {
//
//    }

    auto const getPoses = [](std::vector<double> const& vec) {
        std::vector<std::array<double, 2>> poses;
        assert(vec.size() % 2 == 0);
        for (auto i = 0; i < vec.size(); i += 2) {
            poses.push_back({vec[i], vec[i + 1]});
        }
        return poses;
    };

    std::ifstream words_stream(words_path);
    auto wordsJson = nlohmann::json::parse(words_stream);

    auto const& extractor_info = wordsJson["extractor_info"];
    int V;
    if (extractor_info.contains("parameters")) V = extractor_info["parameters"]["V"];
    else {
        bool const use_hue = extractor_info["use_hue"].get<bool>();
        bool const use_intensity = extractor_info["use_intensity"].get<bool>();
        bool const use_texton = extractor_info["use_texton"].get<bool>();
        bool const use_orb = extractor_info["use_orb"].get<bool>();
        V = 180 * use_hue + 256 * use_intensity + 1000 * use_texton + 15000 * use_orb;
    }

    auto const x0 = wordsJson["map_info"]["x0"].get<double>();
    auto const x1 = wordsJson["map_info"]["x1"].get<double>();
    auto const y0 = wordsJson["map_info"]["y0"].get<double>();
    auto const y1 = wordsJson["map_info"]["y1"].get<double>();
    std::string const size_spec = std::to_string(x0) + "x" + std::to_string(y0) + "x" + std::to_string(x1 - x0) + "x" + std::to_string(y1 - y0);

    sunshine::Parameters params{{
        {"K", K},
        {"V", V},
        {"world_frame", std::string("world")},
        {"min_obs_refine_time", 0},
        {"alpha", alpha},
        {"beta", beta},
        {"gamma", gamma},
        {"cell_space", cell_space},
        {"num_threads", 4},
        {"cell_time", cell_time},
        {"G_space", 2}
    }, true};
    sunshine::WordColorMap<int> colorMap;
    std::vector<CategoricalObservation<int, 2, double>> wordObservations;
    {
        boost::progress_display bar(wordsJson["word_observations"].size());
        for (auto const& obs : wordsJson["word_observations"]) {
            wordObservations.emplace_back(
                obs["frame"].get<std::string>(),
                obs["timestamp"].get<double>(),
                obs["id"].get<uint32_t>(),
                obs["observations"].get<std::vector<int>>(),
                getPoses(obs["observation_poses"].get<std::vector<double>>()),
                obs["vocabulary_start"].get<uint64_t>(),
                obs["vocabulary_size"].get<uint64_t>()
            );
            ++bar;
        }
    }

    ros::Time::init();
    for (auto c = 0; c < count; ++c) {
        ROSTAdapter<3> adapter(&params, nullptr, {}, false);
        boost::progress_display bar(wordsJson["word_observations"].size());
        for (auto const& obs : wordObservations) {
            adapter(&obs);
            ++bar;
        }

        std::cout << "Waiting for model " << c << " convergence" << std::endl;
        TopicMapPtr map;
        auto const end_time = std::chrono::steady_clock::now() + std::chrono::seconds(200);
        while (std::chrono::steady_clock::now() < end_time) {
            std::this_thread::sleep_for(std::chrono::seconds(3));
            {
                auto const& readToken = adapter.get_rost().get_read_token();
                save_topics_csv(adapter, output_prefix + std::to_string(c) + "_map.csv", false);
                map = generate_topic_map(adapter, adapter.get_last_observation_time(), false, false, 1);
            }
            auto const& mapImg = createTopicImg<Pose2D>(*map, colorMap, cell_space, true, 0, 0, size_spec, 10);
            cv::imshow("map" + std::to_string(c), mapImg);
            cv::waitKey(10);

            saveTopicImg(mapImg, output_prefix + std::to_string(c) + "_map.png");
        }
    }
    return 0;
}