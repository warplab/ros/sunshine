//
// Created by stewart on 12/19/23.
//

#include "sunshine/common/parameters.hpp"
#include "sunshine/external/json.hpp"
#include "sunshine/visual_word_adapter.hpp"

using namespace sunshine;
using json = nlohmann::ordered_json;

int main(int argc, char** argv) {
    if (argc < 4) {
        std::cerr << "Usage: ./utils.dataset_to_words <odometry.json> <image_dir> [depth_dir]" << std::endl;
        return 1;
    }
}