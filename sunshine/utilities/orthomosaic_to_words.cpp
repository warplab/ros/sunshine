//
// Created by stewart on 1/11/23.
//

#include <fstream>
#include "sunshine/common/parameters.hpp"
#include "sunshine/external/json.hpp"
#include "sunshine/visual_word_adapter.hpp"
#include <opencv2/highgui.hpp>

using namespace sunshine;
using namespace std::chrono;
using json = nlohmann::ordered_json;

void splitColorAndAlpha(cv::Mat const& bgra, cv::Mat& bgr, cv::Mat& alpha) {
    bgr = cv::Mat(bgra.rows, bgra.cols, CV_8UC3);
    alpha = cv::Mat(bgra.rows, bgra.cols, CV_8UC1);

    std::array<cv::Mat, 2> out = {bgr, alpha};
    // bgra[0] -> bgr[0], bgra[1] -> bgr[1],
    // bgra[2] -> bgr[2], bgra[3] -> alpha[0]
    static constexpr std::array<int, 8> from_to = { 0,0, 1,1, 2,2, 3,3 };
    mixChannels(&bgra, 1, out.data(), 2, from_to.data(), 4);
}

int main(int argc, char** argv) {
    if (argc != 10) {
        std::cerr << "Usage: ./utils.orthomosaic_to_words <image_file.ext> <x0> <x1> <y0> <y1> <obs_width_m> <obs_height_m> <step_size> "
                     "<output_file.json>"
                  << std::endl;
        return 1;
    }
    std::string const image_path(argv[1]);
    double const x0        = std::stod(argv[2]);
    double const x1        = std::stod(argv[3]);
    double const y0        = std::stod(argv[4]);
    double const y1        = std::stod(argv[5]);
    double const view_x    = std::stod(argv[6]);
    double const view_y    = std::stod(argv[7]);
    double const step_size = std::stod(argv[8]);
    std::string const output_path(argv[9]);

    if (x0 >= x1 || y0 >= y1)
        throw std::invalid_argument("Invalid x0,y0 or x1,y1");
    else if (view_x <= 0 || view_y <= 0)
        throw std::invalid_argument("Invalid obs width or height");
    else if (step_size <= 0)
        throw std::invalid_argument("Invalid cell size");

    double const x_origin = x0 + (step_size / 2);
    double const y_origin = y0 + (step_size / 2);

    double const map_w = x1 - x0;
    double const map_h = y1 - y0;
    int32_t const xi0  = 0;
    int32_t const yi0  = 0;
    int32_t const xi1  = std::lround(std::nextafter((x1 - x_origin) / step_size, 0.));
    int32_t const yi1  = std::lround(std::nextafter((y1 - y_origin) / step_size, 0.));
    int32_t const rows = yi1 - yi0 + 1;
    int32_t const cols = xi1 - xi0 + 1;

    bool const use_hue    = true;
    bool const use_intensity = true;
    bool const use_clahe  = false;
    bool const use_texton = false;
    bool const use_orb    = true;
    bool const color_correct = true;
    sunshine::Parameters params{{{"V", 180 * use_hue + 256 * use_intensity + 1000 * use_texton + 15000 * use_orb},
                                 {"use_hue", use_hue},
                                 {"use_intensity", use_intensity},
                                 {"use_clahe", use_clahe},
                                 {"use_texton", use_texton},
                                 {"use_orb", use_orb},
                                 {"color_correction", color_correct}}};

    json results;
    {
        cv::Mat bgr, alpha;
        auto const base_image = cv::imread(image_path, cv::IMREAD_UNCHANGED);
        if (base_image.empty()) { throw std::invalid_argument("Empty image"); }
        else if (base_image.channels() < 3) throw std::invalid_argument("Color image required");
        else if (base_image.channels() > 3) {
            if (base_image.channels() > 4) throw std::invalid_argument("Unrecognized image channel configuration");
            splitColorAndAlpha(base_image, bgr, alpha);
        } else {
            bgr = base_image;
            assert(alpha.empty());
        }
        double const rx   = (base_image.cols / map_w);
        double const ry   = (base_image.rows / map_h);
        uint32_t const iw = std::round(rx * view_x);
        uint32_t const ih = std::round(ry * view_y);

        std::cout << "Creating JSON dataset of " << rows * cols << " " << iw << "x" << ih << " visual observations" << std::endl;

        //        auto const image_to_world = [&rx, &ry](int32_t const& ix0, int32_t const& iy1, int32_t const& px, int32_t const& py) {
        //            return std::make_pair(px * rx + ix0, iy1 - py * ry);
        //        };

        results["map_info"]["x0"]                          = x0;
        results["map_info"]["y0"]                          = y0;
        results["map_info"]["x1"]                          = x1;
        results["map_info"]["y1"]                          = y1;
        results["map_info"]["step_size"]                   = step_size;
        results["map_info"]["rows"]                        = rows;
        results["map_info"]["cols"]                        = cols;
        results["map_info"]["cells"]                       = rows * cols;
        results["extractor_info"]["base_image_path"]       = image_path;
        results["extractor_info"]["base_image_resolution"] = std::to_string(base_image.cols) + "x" + std::to_string(base_image.rows);
        results["extractor_info"]["obs_width_world"]       = view_x;
        results["extractor_info"]["obs_height_world"]      = view_y;
        results["extractor_info"]["obs_resolution"]        = std::to_string(iw) + "x" + std::to_string(ih);
        json results_array                                 = json::array();
        auto const timestamp = duration_cast<duration<double>>(system_clock::now().time_since_epoch()).count();
        VisualWordAdapter wordAdapter(&params);
        results["extractor_info"]["parameters"]            = params.toJson(); // needs to be done after wordAdapter initialization
        uint32_t seq     = 0;
        auto const start = steady_clock::now();
        for (int32_t yi = yi1; yi >= yi0; --yi) {
            auto const cy          = y_origin + yi * step_size;
            auto const top_w       = cy + view_y / 2.;
            auto const offset_down = std::max(0., top_w - y1);
            auto const trunc_top_w = top_w - offset_down;

            int32_t const top_px    = std::lround(std::max(0., (y1 - top_w) * ry));
            int32_t const bottom_px = std::min(base_image.rows, (int) std::max(0., std::round((y1 - top_w) * ry + ih)));

            for (int32_t xi = xi0; xi <= xi1; ++xi) {
                auto const cx           = xi * step_size + x_origin;
                auto const left_w       = cx - view_x / 2.;
                auto const offset_right = std::max(0., x0 - left_w);
                auto const trunc_left_w = left_w + offset_right;

                int32_t const left_px  = (int) std::round(std::max(0., (left_w - x0) * rx));
                int32_t const right_px = std::min(base_image.cols, (int) std::max(0., std::round((left_w - x0) * rx + iw)));
                assert(left_px < right_px && top_px < bottom_px);
                assert(right_px - left_px <= iw && bottom_px - top_px <= ih);

                cv::Range const vertical(top_px, bottom_px), horizontal(left_px, right_px);
                cv::Mat const& image = bgr(vertical, horizontal);
                cv::Mat const& alpha_mask = (alpha.empty()) ? alpha : alpha(vertical, horizontal);
#ifndef NDEBUG
                cv::imshow("debug", image);
                cv::waitKey(100);
#endif
                auto const& wordObs = wordAdapter(std::make_unique<ImageObservation>("world", timestamp, seq++, image));
                std::vector<int> observations;
                if (alpha.empty()) observations = wordObs->observations;
                else observations.reserve(wordObs->observations.size());
                std::vector<double> observation_poses_w;
                observation_poses_w.reserve(wordObs->observation_poses.size() * 2);
                for (auto i = 0; i < wordObs->observations.size(); ++i) {
                    auto const& pose = wordObs->observation_poses[i];
                    if (!alpha_mask.empty()) {
                        auto const& a = alpha_mask.at<uint8_t>(pose[1], pose[0]);
                        if (a == 0) continue;
                        assert(a == 255);
                        observations.push_back(wordObs->observations[i]);
                    }
                    assert(pose[0] >= 0 && pose[0] < iw);
                    assert(pose[1] >= 0 && pose[1] < ih);
                    double const world_x = trunc_left_w + pose[0] / rx;
                    double const world_y = trunc_top_w - pose[1] / ry;
                    assert(world_x >= x0 && world_x < x1);
                    assert(world_y >= y0 && world_y < y1);
                    observation_poses_w.push_back(world_x);
                    observation_poses_w.push_back(world_y);
                }
                json obsDict;
                obsDict["cell_coords"]           = std::array<int, 2>{xi, yi};
                obsDict["cell_center_world_loc"] = std::array<double, 2>{cx, cy};
                obsDict["frame"]                 = wordObs->frame;
                obsDict["timestamp"]             = wordObs->timestamp;
                obsDict["id"]                    = wordObs->id;
                obsDict["pose_dim"]              = wordObs->PoseDim;
                obsDict["vocabulary_start"]      = wordObs->vocabulary_start;
                obsDict["vocabulary_size"]       = wordObs->vocabulary_size;
                obsDict["observations"]          = observations;
                obsDict["observation_poses"]     = observation_poses_w;
                results_array.push_back(obsDict);

                if (seq > 1) std::cout << "\r";
                double const rate = static_cast<double>(seq) / duration_cast<seconds>(steady_clock::now() - start).count();
                std::printf("Done %6d of %d (%.2f cells/s, ETC %.0f s)", seq, rows * cols, rate, (rows * cols - seq) / rate);
                std::cout << std::flush;
            }
        }
        std::cout << std::endl;
        results["word_observations"] = results_array;
    }
    std::ofstream resultsWriter(output_path);
    resultsWriter << results.dump(2);
    resultsWriter.close();
}
