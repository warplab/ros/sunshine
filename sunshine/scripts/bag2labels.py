#!/usr/bin/env python

import argparse

from tqdm import tqdm
from collections import namedtuple

import rosbag


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Extract images from a ROS bag.")
    parser.add_argument("bag_file", help="Input ROS bag.")
    parser.add_argument("robot_name", help="Robot name")
    parser.add_argument("--query_topic", default="/sil/query_topic", help="Query topic(s).")
    parser.add_argument("--label_topic", default="/sil/label_topic", help="Label topic(s).")
    parser.add_argument("--format_str", default="{secs}-{nsecs:09d}.jpg")
    parser.add_argument("--header", default="Image Filename,User Input,Query Stamp,Query Age,Label Delay")

    args = parser.parse_args()

    bag = rosbag.Bag(args.bag_file, "r")
    Info = namedtuple('info', 'header selection_method query_stamp')
    image_infos = {}
    image_labels = {}

    for topic, msg, t in tqdm(bag.read_messages(topics=[args.query_topic, args.label_topic]), desc="Processing messages"):
        if topic == args.query_topic:
            image_infos[msg.uuid] = Info(header=msg.image.header, selection_method=msg.selection_method, query_stamp=t), None
        elif topic == args.label_topic:
            image_labels.clear()
            for uuid, label in zip(msg.image_uuids, msg.interest_values):
                image_labels[uuid] = label
                if uuid in image_infos and image_infos[uuid][1] is None:
                    image_infos[uuid] = image_infos[uuid][0], t

    print(args.header)
    for uuid, label in sorted(image_labels.items(), key=lambda x: image_infos[x[0]][1]):
        info, label_stamp = image_infos[uuid]
        key = args.format_str.format(secs=info.header.stamp.secs, nsecs=info.header.stamp.nsecs)
        print(",".join([key, str(label), str(info.query_stamp.to_sec()),
                        str((info.query_stamp - info.header.stamp).to_sec()),
                        str((label_stamp - info.query_stamp).to_sec())]))

    bag.close()
