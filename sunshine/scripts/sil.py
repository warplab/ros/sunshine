#!/usr/bin/env python3
from typing import Dict, Any

import rospy
import rospkg
import numpy as np
from typing import Tuple
from sensor_msgs.msg import CompressedImage
from std_msgs.msg import Float64, Int32, Float32MultiArray
from warpauv_msgs.msg import LabelRequest, InterestLabels
from warpauv_msgs.srv import SetDriveSpeed, SetDriveSpeedRequest
from sunshine_msgs.msg import DenseTopicMap
from sunshine_msgs.srv import SetRegion, SetRegionResponse
from nav_msgs.msg import Path
from tf2_geometry_msgs import PointStamped, PoseStamped
from geometry_msgs.msg import PointStamped, PoseStamped
import sensor_msgs.point_cloud2 as pc12
import tf2_ros
from tf2_ros import TransformListener, Buffer

import math
from query_selectors import *

import numpy as np

def discretizePath(path, cell_size, first_half=False):
    waypoints = path[:len(path)//2] if first_half else path
    diffs = np.concatenate((np.zeros((1,2)), np.diff(waypoints, axis=0)), axis=0)
    dists = np.sqrt(np.sum(np.square(diffs), axis=1))
    waypoint_ts = np.cumsum(dists)
    waypoint_idxs = (waypoint_ts / cell_size).astype(int)
    T = waypoint_ts[-1]
    ts = np.arange(0, T, cell_size)
    ts = np.concatenate((ts, [T]), axis=0)
    xs = []
    ys = []
    for t in ts:
        xs.append(np.interp(t, waypoint_ts, waypoints[:, 0]))
        ys.append(np.interp(t, waypoint_ts, waypoints[:, 1]))
    return np.concatenate((np.array(xs)[:, np.newaxis], np.array(ys)[:, np.newaxis]), axis=1), waypoint_idxs

class ScientistInLoop():
    image_cache: Dict[int, Tuple[float, CompressedImage]]
    map: np.ndarray

    obs_length = 2.
    high_speed = 1.
    low_speed = 0.25
    dl = 10
    discount_factor = .95
    query_period = 60
    config = EBRMConfig

    def __init__(self):
        robot_name = rospy.get_param('~robot_name', 'warpauv_3')
        image_topic = rospy.get_param('~image_topic', f"/{robot_name}/cameras/downward/left/image_color/compressed")
        self.budget = rospy.get_param('~budget', 100)

        self.last_idx = 0
        self.X, self.Y = None, np.zeros((0,))

        mission_topic = f"/{robot_name}/mission_control/full_mission"
        leg_topic = f"/{robot_name}/mission_control/leg_number"
        speed_service = f"/{robot_name}/mission_control/drive_speed"
        topic_map = f"map_extrapolator/extrapolated_map"
        query_topic = "query_topic"
        label_topic = "label_topic"
        self.base_frame = f"{robot_name}/base_link"
        weights_topic = "weights"
        plan_topic = "plan"
        history_topic = "history"

        self.last_mission = None
        self.mission_origin = None
        self.mission_size = None
        self.on_track_poses = None
        self.current_target = None
        self.model = None
        self.training_set = None
        self.executed = None
        self.policy = None

        self.tf_buffer = Buffer()
        self.tf_listener = TransformListener(self.tf_buffer)

        self.world_frame = f"{robot_name}/map"

        self.image_cache = {}
        self.visited_idxs = set()
        self.unlabeled_idxs = set()
        self.labeled_idxs = list()
        self.labels = list()
        self.pose_topics = None
        self.map = None
        self.leg = None
        self.waypoint_idxs = None
        self.next_waypoint_idx = None
        self.current_speed = self.high_speed

        self.mission_sub = rospy.Subscriber(mission_topic, Path, self.missionCallback)
        self.leg_sub = rospy.Subscriber(leg_topic, Int32, self.legCallback)
        self.map_sub = rospy.Subscriber(topic_map, DenseTopicMap, self.mapCallback)
        self.img_sub = rospy.Subscriber(image_topic, CompressedImage, self.imageCallback)
        self.query_pub = rospy.Publisher(query_topic, LabelRequest, queue_size=10)
        self.label_sub = rospy.Subscriber(label_topic, InterestLabels, self.labelCallback)
        self.speed_service = rospy.ServiceProxy(speed_service, SetDriveSpeed)
        self.extrapolation_service = rospy.ServiceProxy('map_extrapolator/set_region', SetRegion)

        self.weight_pub = rospy.Publisher(weights_topic, Float32MultiArray, queue_size=3)
        self.plan_pub = rospy.Publisher(plan_topic, Float32MultiArray, queue_size=3)
        self.history_pub = rospy.Publisher(history_topic, Float32MultiArray, queue_size=3)

        self.last_query = None
        self.last_query_time = rospy.get_time()
        self.update_timer = rospy.Timer(rospy.Duration(1), self.timerCallback)

    def missionCallback(self, mission):
        """
        :type mission: Path
        """
        if self.last_mission is not None and self.last_mission.poses == mission.poses:
            return
        elif self.last_mission is not None:
            rospy.logerr('Already have a mission -- please kill SIL node and restart')
            return
        p: PoseStamped
        x0, x1, y0, y1 = np.inf, -np.inf, np.inf, -np.inf
        path = []
        for p in mission.poses:
            if p.header.frame_id != self.world_frame:
                p = self.tf_buffer.transform(p, self.world_frame, rospy.Duration(0))
            x0 = min(x0, p.pose.position.x, x0)
            x1 = max(x1, p.pose.position.x, x1)
            y0 = min(y0, p.pose.position.y, y0)
            y1 = max(y1, p.pose.position.y, y1)
            path.append((p.pose.position.x, p.pose.position.y))
        mission_origin = x0 - self.obs_length / 2, y0 - self.obs_length / 2
        mission_size = (x1 - x0) + self.obs_length, (y1 - y0) + self.obs_length
        rospy.loginfo(f"Setting mission to origin {mission_origin} with size {mission_size} (was {self.mission_origin} with size {self.mission_size})")
        self.mission_origin = mission_origin
        self.mission_size = mission_size
        while not rospy.is_shutdown():
            try:
                self.extrapolation_service.wait_for_service(timeout=3)
                origin = self.mission_origin
                size = self.mission_size
                response: SetRegionResponse = self.extrapolation_service.call(*origin, *size)
                if response.success:
                    break
                else:
                    rospy.logerr("Extrapolation service request failed, retrying")
            except rospy.ROSException:
                rospy.logwarn("Waiting for extrapolation service")
                pass

        self.on_track_poses, self.waypoint_idxs = discretizePath(np.array(path), self.obs_length)
        if self.leg is not None:
            self.next_waypoint_idx = self.waypoint_idxs[self.leg+1]
        self.executed = np.zeros((len(self.on_track_poses),))
        self.policy = None
        self.last_mission = mission

    def legCallback(self, leg_msg):
        """
        :type leg_msg: Int32
        """
        if self.waypoint_idxs is None:
            rospy.logwarn_throttle(2, 'Cannot set leg number without mission')
            return
        if self.leg == leg_msg.data:
            return
        self.leg = leg_msg.data
        if self.waypoint_idxs is not None:
            self.next_waypoint_idx = self.waypoint_idxs[self.leg+1]

    def toPoseIdx(self, x, y):
        loc = np.array([x, y])
        diffs = self.on_track_poses[self.last_idx:, :] - loc[np.newaxis, :]
        dists = np.sum(np.abs(diffs), axis=1)
        idx = int(np.argmin(dists)) + self.last_idx
        return idx, np.min(dists)

    def imageCallback(self, img_data):
        """

        :type img_data: CompressedImage
        """
        if self.on_track_poses is None:
            rospy.logwarn('Mission not yet received; ignoring image')
            return
        pose = self.tf_buffer.lookup_transform(self.world_frame, img_data.header.frame_id, img_data.header.stamp)
        idx, dist = self.toPoseIdx(pose.transform.translation.x, pose.transform.translation.y)
        # rospy.logwarn(f"{pose.transform.translation} {idx, dist}")
        if dist > self.obs_length:
            rospy.logwarn_throttle(2, 'Image is far from any pose, ignoring')
            return # ignore
        elif idx in self.labeled_idxs:
            return # ignore
        elif idx not in self.image_cache.keys() or dist < self.image_cache[idx][0]:
            self.image_cache[idx] = (dist, img_data)
            if idx not in self.visited_idxs:
                self.unlabeled_idxs.add(idx)
            self.visited_idxs.add(idx)
            rospy.logdebug(f'Updated image for pose idx {idx}')

    def updateModel(self):
        if len(self.labeled_idxs) == 0:
            rospy.logwarn_throttle(2, 'Cannot update model; waiting for labels')
            return
        elif self.pose_topics is None:
            rospy.logwarn_throttle(2, 'Cannot update model; waiting for topics')
            return
        self.X = np.stack([self.pose_topics[i, :] for i in self.labeled_idxs], axis=0)
        self.Y = np.array(self.labels)
        self.model = train_model(self.X, self.Y)
        self.updatePlan()
        self.weight_pub.publish(data=self.model.coef_)

    def updatePlan(self):
        """

        :type y: np.ndarray
        """
        if self.next_waypoint_idx is None:
            rospy.logwarn_throttle(2, 'Cannot update plan without knowing next waypoint!')
            return
        elif self.model is None:
            rospy.logwarn_throttle(2, 'Cannot update plan without model!')
            return
        policy = np.zeros((len(self.on_track_poses[self.last_idx+1:]),)) + self.high_speed
        if self.budget == 0:
            rospy.logwarn_once('Budget empty; setting high speed for remainder')
            self.policy = policy
            return
        
        if self.last_idx is not None and self.budget >= len(self.pose_topics) - self.last_idx:
            rospy.logwarn_once('Excess budget (%d poses, %d current); setting low speed for remainder', len(self.pose_topics), self.last_idx)
            self.policy = policy - self.high_speed + self.low_speed
            return
        # rospy.loginfo_throttle(2, 'Updating plan')
        policy = self.config.planner(model=self.model,
                                     query=self.last_query,
                                     budget=self.budget,
                                     x_train=self.X,
                                     y_train=self.Y,
                                     X=self.pose_topics,
                                     t=self.last_idx+1,
                                     dq=self.dl,
                                     dl=self.dl,
                                     discount_factor=self.discount_factor)
        sample_locs = policy == 1
        if np.sum(policy) > self.budget:
            rospy.logerr_throttle(2, 'Policy exceeds budget!')
        policy = np.ones_like(policy) * self.high_speed
        policy[sample_locs] = self.low_speed
        # policy[~sample_locs] = self.high_speed
        # X = self.get_predicted_topics()
        # y = self.model.predict(X)
        # best_idxs = np.argpartition(y, -self.budget)[-self.budget:] + self.next_waypoint_idx
        # policy[best_idxs] = self.low_speed
        self.policy = policy
        self.plan_pub.publish(data=policy.tolist())

    def updateSpeed(self):
        if self.policy is None:
            rospy.logwarn_throttle(2, "No policy; can't update speed")
            return
        elif self.next_waypoint_idx is None:
            rospy.logwarn_throttle(2, "Don't know next waypoint; can't update speed")
            return
        self.policy[self.policy == 0] = self.high_speed
        latest_pose = self.tf_buffer.lookup_transform(self.world_frame, self.base_frame, rospy.Time())
        idx, dist = self.toPoseIdx(latest_pose.transform.translation.x, latest_pose.transform.translation.y)
        if idx < 0 or idx >= len(self.on_track_poses):
            rospy.logerr(f'Invalid idx {idx} (expected {len(self.on_track_poses)} poses)')
            return
        elif dist > self.obs_length:
            rospy.logwarn(f'Inferred pose {latest_pose.transform.translation} outside observation distance ({dist}); ignoring')
            return
        elif self.last_idx is not None and idx < self.last_idx:
            rospy.logwarn_throttle(2, 'Inferred location is backwards; ignoring')
            return
        elif idx > self.next_waypoint_idx:
            rospy.logwarn_throttle(2, 'Location is too far forwards! Skipping speed update')
            return
        elif idx == self.last_idx:
            return  # callback happened too quick
        if self.current_speed == self.low_speed:
            rospy.loginfo("Reducing budget by %d", idx - self.last_idx + 1)
            self.budget = max(self.budget - (idx - self.last_idx + 1), 0)
            if self.budget < 0:
                rospy.logerr_once(f'Overran budget; whoops (current value: {self.budget})')
        self.executed[self.last_idx:idx + 1] = self.current_speed
        self.history_pub.publish(data=self.executed.tolist())
        if idx < len(self.on_track_poses) - 1:
            self.current_speed = self.policy[idx-(self.last_idx+1)]
            rospy.loginfo(f'Setting speed for position index {idx+1} to {self.current_speed}')
            try:
                self.speed_service.wait_for_service(1)
            except rospy.ROSException:
                rospy.logerr(f'Failed to set new speed; speed service not available')
                return
            request = SetDriveSpeedRequest()
            request.speed = self.current_speed
            self.speed_service.call(request)
            self.last_idx = idx
        else:
            rospy.logerr_once('SIL Mission over!')

    def labelCallback(self, labels):
        """
        :type labels: InterestLabels
        """
        self.labeled_idxs = labels.image_uuids
        self.labels = labels.interest_values
        self.updateModel()

    def mapCallback(self, topic_map):
        """
        :type topic_map: DenseTopicMap
        """
        self.map_msg = topic_map
        if self.on_track_poses is None:
            rospy.logwarn('Map received but no mission --- waiting...')
            return
        map_size = np.array([topic_map.info.width, topic_map.info.height, -1])
        self.map_topics = np.squeeze(np.array(topic_map.map_topic_proportions).reshape(map_size))
        assert self.mission_size[0] / topic_map.spatial_cell_size[0] == map_size[0]
        assert self.mission_size[1] / topic_map.spatial_cell_size[1] == map_size[1]


        # cell_centers_x = np.arange(self.mission_origin[0], self.mission_origin[0] + self.mission_size[0] - topic_map.spatial_cell_size[0] / 2, topic_map.spatial_cell_size[0]) + 0.5
        # cell_centers_y = np.arange(self.mission_origin[1], self.mission_origin[1] + self.mission_size[1] - topic_map.spatial_cell_size[1] / 2, topic_map.spatial_cell_size[1]) + 0.5
        # # cell_centers = np.stack((cell_centers_x, cell_centers_y), axis=1) + 0.5
        #
        # dx = cell_centers_x[np.newaxis, :] - self.on_track_poses[:, 0:1]
        # dy = cell_centers_y[np.newaxis, :] - self.on_track_poses[:, 1:2]
        # distances = np.sqrt(np.square(dx) + np.square(dy))
        # closest_idxs = np.argmin(distances, axis=1)
        # self.K = self.map_topics.shape[2]

        self.K = self.map_topics.shape[2]
        # cells_per_track_pose = int(round(max(self.obs_length / topic_map.spatial_cell_size[0], 1))), int(round(max(self.obs_length / topic_map.spatial_cell_size[1], 1)))
        spatial_cell_size = np.array(topic_map.spatial_cell_size[:2])[np.newaxis, :]
        map_centers = self.on_track_poses - np.array(self.mission_origin)
        map_bl_idxs = np.maximum(np.round((map_centers - (self.obs_length / 2)) / spatial_cell_size), np.array([[0, 0]])).astype(int)
        map_tr_idxs = np.minimum(np.round((map_centers + (self.obs_length / 2)) / spatial_cell_size), map_size[np.newaxis, :2]).astype(int)
        self.pose_topics = np.ones((self.on_track_poses.shape[0], self.K)) / self.K
        for i in range(len(self.on_track_poses)):
            bl, tr = map_bl_idxs[i, :], map_tr_idxs[i, :]
            if tr[0] == bl[0]:
                rospy.logwarn_once('Adjusting map cell for non-zero x size')
                if bl[0] > 0:
                    bl[0] -= 1
                else:
                    tr[0] += 1
            if tr[1] == bl[1]:
                rospy.logwarn_once('Adjusting map cell for non-zero y size')
                if bl[1] > 0:
                    bl[1] -= 1
                else:
                    tr[1] += 1
            self.pose_topics[i, :] = self.map_topics[bl[0]:tr[0], bl[1]:tr[1], :].mean(axis=(0,1))
        self.pose_topics = np.divide(self.pose_topics, np.sum(self.pose_topics, axis=-1)[:, np.newaxis])
        rospy.logdebug('Updated topic map')
        self.updateModel()

    # def get_predicted_topics(self):
    #     if self.pose_topics is None:
    #         rospy.logerr('No map yet! Cannot predict topics')
    #         return None
    #     elif self.next_waypoint_idx is None:
    #         rospy.logerr('Unknown position along path! Waiting for leg number')
    #         return None
    #     return self.pose_topics[self.next_waypoint_idx:, :]

    def timerCallback(self, timerEvent):
        self.updateSpeed()
        if rospy.get_time() - self.last_query_time < self.query_period:
            return
        elif len(self.unlabeled_idxs) == 0:
            rospy.logwarn_throttle(self.query_period, 'No unlabeled queries --- delaying query selection')
            return
        # topic_predictions = self.get_predicted_topics()
        query, value = self.config.query_selector(model=self.model,
                                           unlabeled=list(self.unlabeled_idxs),
                                           budget=self.budget,
                                           x_train=self.X,
                                           y_train=self.Y,
                                           X=self.pose_topics,
                                           t=self.last_idx,
                                           dq=self.dl,
                                           dl=self.dl,
                                           discount_factor=self.discount_factor)
        self.unlabeled_idxs.remove(query)
        rospy.loginfo(f'Querying idx: {query}')
        request = LabelRequest()
        request.selection_method = self.config.name
        request.image = self.image_cache[query][1]
        request.uuid = query
        # request.obs_centre =
        # request.obs_volume =
        self.query_pub.publish(request)
        self.last_query = query
        self.last_query_time = rospy.get_time()


if __name__ == '__main__':
    try:
        rospy.init_node("sil-manager")
        node = ScientistInLoop()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
