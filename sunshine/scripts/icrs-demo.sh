#!/bin/sh
OUTPUT_DIR=$PWD/icrs-3d-$(date +%s)
mkdir "$OUTPUT_DIR"
tmux new-session -d 'cd ~/warp_ws && source devel/setup.bash && roslaunch warpauv_config rgbdepth_to_pointcloud.launch camera_ns:=/warpauv_1/cameras/mapping_cam out_cloud:=/warpauv_1/cameras/mapping_cam/points color_correct_images:=True; bash'
tmux split-window -v "cd ~/warp_ws && source devel/setup.bash && sleep 2 && roslaunch sunshine warpauv_bag.launch world_frame:=warpauv_1/odom robot_name:=warpauv_1 bagfile:=/data/datasets/2021-10-USVI/data/2021-10-26/WARPAUV_1/warpauv_1_xavier1_2021-10-26-10-33-31.bag bag_start:=450 only_play_images:=true odom_topic:=/odom use_rviz:=true texton:=true orb:=true gamma:=0 K:=5 V:=16436 clahe:=false num_threads:=4 cell_size:=3600x0.3048x0.3048x0.3048 cell_space:=0.3048 visualize_2d:=true maps_dir:=\"$OUTPUT_DIR\" topics_dir:=\"$OUTPUT_DIR\"; bash"
#tmux split-window -v 'cd ~/warp_ws && source devel/setup.bash && sleep 2 && roslaunch sunshine warpauv_zed_bag.launch robot_name:=warpauv_1 /data/2021-usvi/2021-10-25/surveys/warpauv_1_xavier1_2021-10-25-12-08-59.bag bag_start:=1500 use_rviz:=true cell_size:=3600x1.2x1.2x15; bash'
# /data/2021-usvi/2021-10-28/surveys/warpauv_1_xavier1_2021-10-28-16-08-21.bag bag_start:=1000
# /data/2021-usvi/2021-10-25/surveys/warpauv_1_xavier1_2021-10-25-12-08-59.bag bag_start:=600
tmux split-window -h 'cd ~/warp_ws && source devel/setup.bash && sleep 2 && roslaunch warpauv_config control_station.launch robot_name:=warpauv_1; bash'
tmux select-pane -t 0
tmux split-window -h "read -p 'Press enter to save topics csv...'; rosservice call /rost/save_topics_by_time_csv \"filename: '$OUTPUT_DIR/topics_by_time.csv'\"; rosservice call /rost/save_topics_by_cell_csv \"filename: '$OUTPUT_DIR/topics_by_cell.csv'\"; rosservice call /rost/save_ppx_by_cell_csv \"filename: '$OUTPUT_DIR/ppx_by_cell.csv'\"; bash"
# tmux split-window -h 'cd ~/warp_ws && source devel/setup.bash && sleep 2 && roslaunch voxblox_ros rgbd_dataset.launch play_bag:=false voxel_size:=0.1 pointcloud_topic:=/warpauv_1/cameras/mapping_cam/points; bash'
sleep 4
rostopic pub -1 /tf_static tf2_msgs/TFMessage "transforms:
- header:
    seq: 0
    stamp:
      secs: 0
      nsecs: 0
    frame_id: 'warpauv_1/base_link'
  child_frame_id: 'mapping_cam_left_camera_optical_frame'
  transform:
    translation:
      x: 0.0
      y: 0.0
      z: 0.0
    rotation:
      x: 0.0
      y: 0.0
      z: 0.0
      w: 1.0"
rostopic pub -1 /tf_static tf2_msgs/TFMessage "transforms:
- header:
    seq: 0
    stamp:
      secs: 0
      nsecs: 0
    frame_id: 'warpauv_1/map'
  child_frame_id: 'world'
  transform:
    translation:
      x: 0.0
      y: 0.0
      z: 0.0
    rotation:
      x: 0.0
      y: 0.0
      z: 0.0
      w: 1.0"
rostopic pub -1 /tf_static tf2_msgs/TFMessage "transforms:
- header:
    seq: 0
    stamp:
      secs: 0
      nsecs: 0
    frame_id: 'warpauv_1/map'
  child_frame_id: 'map'
  transform:
    translation:
      x: 0.0
      y: 0.0
      z: 0.0
    rotation:
      x: 0.0
      y: 0.0
      z: 0.0
      w: 1.0"
rostopic pub -1 /tf_static tf2_msgs/TFMessage "transforms:
- header:
    seq: 0
    stamp:
      secs: 0
      nsecs: 0
    frame_id: 'warpauv_1/map'
  child_frame_id: 'warpauv_1/odom'
  transform:
    translation:
      x: 0.0
      y: 0.0
      z: 0.0
    rotation:
      x: 0.0
      y: 1.0
      z: 0.0
      w: 0.0"
tmux -2 attach-session -d
