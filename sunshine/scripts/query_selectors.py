import numpy as np
from math import ceil

import rospy
from sklearn.linear_model import BayesianRidge

def train_model(X, Y):
    model = BayesianRidge(alpha_1=10, alpha_init=10, alpha_2=1, lambda_init=1, lambda_1=1, lambda_2=1,
                          fit_intercept=False)
    # model = BetaRegressionWrapper(train_x.shape[1], 1 if len(train_y.shape) == 1 else train_y.shape[1])
    model.fit(X, Y)
    return model


def greedy_plan(y, budget):
    """

    :type y: np.ndarray
    """
    if budget == 0:
        return np.zeros_like(y, dtype=int)
    if budget >= len(y):
        return np.ones_like(y, dtype=int)
    best_idxs = np.argpartition(y, -budget)[-budget:]
    policy = np.zeros_like(y, dtype=int)
    policy[best_idxs] = 1
    return policy


def greedy_planner(model, X, t, budget, **kwargs):
    return greedy_plan(model.predict(X[t:]), budget)


def reward(policy, y):
    return np.sum(policy * y)


def random_selection(unlabeled, **kwargs):
    if len(unlabeled) == 0:
        return None, 0.
    return np.random.choice(unlabeled), 0.


def ebrm_policy(model, unlabeled, budget, x_train, y_train, X, t, dq, dl, discount_factor):
    """

    :type model: BayesianRidge
    """
    if model is None:
        rospy.logwarn_throttle(2, f'Cannot generate EBRM query without model - choosing random from {unlabeled}')
        return random_selection(unlabeled=unlabeled)
    elif len(unlabeled) == 0:
        rospy.logerr_throttle('No unlabelled data! Nothing to select')
        return None, 0.
    T = X.shape[0]
    assert 0 < dl <= dq
    # assert me >= 0
    idxs = [unlabeled[0]]
    if t+dl > T or budget == 0:
        return idxs[0], 0.
    Xprior = np.stack([X[q, :] for q in unlabeled], axis=0)
    Xpred = X[t:T, :]
    # Xfullpred = [X[T*robot+t:T*(robot+1), :] for robot in robots]

    full_pred = model.predict(Xpred)
    base_policy = greedy_plan(full_pred, budget)
    Xnorm = Xprior / np.linalg.norm(Xprior, ord=2, axis=1)[:, np.newaxis]
    Xnormall = X / np.linalg.norm(X, ord=2, axis=1)[:, np.newaxis]
    candidates = np.concatenate(list(Xnormall[q:q+1,:] for q in unlabeled), axis=0)
    for i in range(1, X.shape[1]):
        sim_scores = np.max(Xnorm @ candidates.T, axis=1)
        best = np.argmin(sim_scores)
        while best not in unlabeled:
            if not np.isfinite(sim_scores[best]):
                rospy.logwarn_throttle(2, 'Failed to find enough candidates')
                break
            sim_scores[best] = np.inf
            best = np.argmin(sim_scores)
        idxs.append(unlabeled[best])
        candidates = np.concatenate((candidates, Xnorm[best:best+1, :]), axis=0)

    # base_pred = model.predict(Xpred)  # USING X HERE IS WRONG!!!!
    # base_reward = reward(base_policy, base_pred)
    # new_model = BayesianRidge(alpha_1=1, alpha_init=1,alpha_2=1, lambda_init=1, lambda_1=1, lambda_2=1)
    scores = []
    baselines = []
    for id in idxs:
        assert 0 <= id <= T
        sample = X[id:id+1, :]
        mean, std = model.predict(sample, return_std=True)
        x_sample = np.concatenate((x_train, sample), axis=0)
        deltas = []
        s_baselines = []
        for sample in range(10):
            y_sample = np.concatenate((y_train, np.random.randn(1) * std + mean))
            new_model = train_model(x_sample, y_sample)
            # new_pred = new_model.predict(X[t:, :])
            # new_policy = greedy_policy(new_pred, budget)
            new_pred = new_model.predict(Xpred)
            new_policy = greedy_plan(new_pred, budget)
            if discount_factor != 1.:
                steps = ceil((T-t) / dq)
                new_rewards = np.zeros((steps,))
                old_rewards = np.zeros((steps,))
                for i in range(steps):
                    a = i*dq
                    b = (i+1)*dq
                    if i == steps - 1:
                        new_rewards[i] = reward(new_policy[a:], new_pred[a:])
                        old_rewards[i] = reward(base_policy[a:], new_pred[a:])
                    else:
                        new_rewards[i] = reward(new_policy[a:b], new_pred[a:b])
                        old_rewards[i] = reward(base_policy[a:b], new_pred[a:b])
                w = np.empty((steps,))
                w[0] = 0.
                w[1] = 1.
                w[2:] = discount_factor
                w[1:] = np.cumprod(w[1:])
                new_reward = np.sum(new_rewards * w)
                old_reward = np.sum(old_rewards * w)
            else:
                new_reward = reward(new_policy, new_pred)
                old_reward = reward(base_policy, new_pred)
            delta = new_reward - old_reward
            deltas.append(delta)
            s_baselines.append(new_policy)
        scores.append(np.mean(deltas))
        baselines.append(np.mean(np.array(s_baselines), axis=0))
    best_score_idx = np.argmax(scores)
    best_score = scores[best_score_idx]

    assert idxs[best_score_idx] in unlabeled
    return idxs[best_score_idx], best_score


def ebrm_planner(model, query, budget, x_train, y_train, X, t, dq, dl, discount_factor, **kwargs):
    """

    :type model: BayesianRidge
    """
    T = X.shape[0]
    assert 0 < dl <= dq
    # assert me >= 0
    if t+dl > T or budget == 0:
        return greedy_plan(model.predict(X[t:T, :]), budget)
    Xpred = X[t:T, :]
    # Xfullpred = [X[T*robot+t:T*(robot+1), :] for robot in robots]

    full_pred = model.predict(Xpred)
    base_policy = greedy_plan(full_pred, budget)

    # base_pred = model.predict(Xpred)  # USING X HERE IS WRONG!!!!
    # base_reward = reward(base_policy, base_pred)
    # new_model = BayesianRidge(alpha_1=1, alpha_init=1,alpha_2=1, lambda_init=1, lambda_1=1, lambda_2=1)

    sample = X[query:query+1, :]
    mean, std = model.predict(sample, return_std=True)
    x_sample = np.concatenate((x_train, sample), axis=0)
    deltas = []
    baseline = []
    for sample in range(10):
        y_sample = np.concatenate((y_train, np.random.randn(1) * std + mean))
        new_model = train_model(x_sample, y_sample)
        # new_pred = new_model.predict(X[t:, :])
        # new_policy = greedy_policy(new_pred, budget)
        new_pred = new_model.predict(Xpred)
        new_policy = greedy_plan(new_pred, budget)
        if discount_factor != 1.:
            steps = ceil((T-t) / dq)
            new_rewards = np.zeros((steps,))
            old_rewards = np.zeros((steps,))
            for i in range(steps):
                a = i*dq
                b = (i+1)*dq
                if i == steps - 1:
                    new_rewards[i] = reward(new_policy[a:], new_pred[a:])
                    old_rewards[i] = reward(base_policy[a:], new_pred[a:])
                else:
                    new_rewards[i] = reward(new_policy[a:b], new_pred[a:b])
                    old_rewards[i] = reward(base_policy[a:b], new_pred[a:b])
            w = np.empty((steps,))
            w[0] = 0.
            w[1] = 1.
            w[2:] = discount_factor
            w[1:] = np.cumprod(w[1:])
            new_reward = np.sum(new_rewards * w)
            old_reward = np.sum(old_rewards * w)
        else:
            new_reward = reward(new_policy, new_pred)
            old_reward = reward(base_policy, new_pred)
        delta = new_reward - old_reward
        deltas.append(delta)
        baseline.append(new_policy)
    baseline = np.mean(np.array(baseline), axis=0)
    budget_percentage = np.sum(baseline[:dl]) / np.sum(baseline)
    local_budget = int(round(budget_percentage * budget))
    rospy.loginfo('Planning with budget %d', budget)
    local_plan = greedy_plan(model.predict(X[t:t + dl, :]), local_budget)
    remote_plan = greedy_plan(full_pred[dl:], budget - local_budget)
    ebrm_plan = np.concatenate((local_plan, remote_plan))

    return ebrm_plan

class EBRMConfig(object):
    query_selector = ebrm_policy
    planner = ebrm_planner
    name = 'EBRM'

class RandomConfig(object):
    query_selector = random_selection
    planner = greedy_planner
    name = 'Random'
