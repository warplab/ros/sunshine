#!/usr/bin/env python3

import rospy
import rospkg
from sensor_msgs.msg import PointCloud2, PointField, Range, Image
from std_msgs.msg import Header
import sensor_msgs.point_cloud2 as pc12

import math

import numpy as np
import struct


class SimPointCloud():
    def __init__(self):
        robot_name = rospy.get_param('~robot_name', 'warpauv_3')
        image_topic = rospy.get_param('~image_topic', f'/{robot_name}/cameras/downward/left_raw/image_rect')
        altitude_topic = rospy.get_param('~altitude_topic', f"/{robot_name}/control/altitude_measurement")

        # Subscribe to altitude message
        self.alt_sub = rospy.Subscriber(altitude_topic, Range, self.altCallback)

        # Subscribe to image message
        self.img_sub = rospy.Subscriber(image_topic, Image, self.imageCallback)

        # Point cloud publisher
        self.depth_pub = rospy.Publisher("/sim_depth_pc", PointCloud2, queue_size=10)

        # Calculate FOV
        self.h_res = None  # rospy.get_param('~h_res', 1280)  # Horizontal resolution used for cloud
        self.v_res = None  # rospy.get_param('~v_res', 720)  # Vertical resolution used for cloud
        self.fx = rospy.get_param('fx', 1028)  # horizontal focal length (from camera calibration matrix K)
        self.fy = rospy.get_param('fy', 1028)  # vertical focal length (from camera calibration matrix K)
        self.calib_h_res = rospy.get_param('h_res_calib', 1920)  # horizontal resolution of calibration matrix K
        self.calib_v_res = rospy.get_param('v_res_calib', 1080)  # vertical resolution of calibration matrix K

        self.h_fov = math.atan(self.calib_h_res / (2 * self.fx)) * 2
        self.v_fov = math.atan(self.calib_v_res / (2 * self.fy)) * 2

        self.altitude = None
        self.pcl = None

        # print("H_FOV: ", math.degrees(self.h_fov))
        # print("V_FOV: ", math.degrees(self.v_fov))

    def altCallback(self, alt_data):
        if self.h_res is None:
            return
        # Assume camera is pointing down along the z-axis
        if alt_data is not None:
            self.altitude = alt_data.range
        elif self.pcl is not None or self.altitude is None:
            return
        # print(alt_data.range)

        # print(points_list)

        # Create point cloud message
        x = np.linspace(-self.h_fov / 2, self.h_fov / 2, self.h_res)
        y = np.linspace(-self.v_fov / 2, self.v_fov / 2, self.v_res)

        px, py = np.meshgrid(x, y)

        Z = np.ones_like(px) * self.altitude
        X = np.tan(px) * self.altitude
        Y = np.tan(py) * self.altitude

        # Flatten
        points = np.column_stack([X.flatten(), Y.flatten(), Z.flatten()])
        points_list = [[pt[0], pt[1], pt[2]] for pt in points]

        header = Header()
        # header.frame_id = "warpauv_2/cameras/downward/left_camera_optical_frame"
        # header.frame_id = "downward_left_camera_optical_frame"

        self.pcl = pc12.create_cloud_xyz32(header, points_list)
        self.pcl.height = self.v_res
        self.pcl.width = self.h_res
        self.pcl.is_dense = True
        self.pcl.row_step = self.h_res * 12

        # fields = [
        # 	PointField('x', 0, PointField.FLOAT32,
        # 	PointField('y', 4, PointField.FLOAT32,
        # 	PointField('z', 8, PointField.FLOAT32)))
        # ]

        # pc2_msg = PointCloud2(header=header, height=1, width=len(points_list))

        # points_list = [[1.0, 1.0, 0.0], [1.0, 2.0, 0.0]]

    def imageCallback(self, img_msg):
        """
        :type img_msg: Image
        """
        # Create meshgrid for pixel coordinates
        # x = np.linspace(-np.tan(self.h_fov / 2), np.tan(self.h_fov / 2), self.h_res)
        # y = np.linspace(-np.tan(self.v_fov / 2), np.tan(self.v_fov / 2), self.v_res)

        self.h_res = img_msg.width
        self.v_res = img_msg.height

        self.altCallback(None)

        if self.pcl:
            self.pcl.header.frame_id = img_msg.header.frame_id
            self.pcl.header.stamp = img_msg.header.stamp

            self.depth_pub.publish(self.pcl)


def main():
    rospy.init_node("sim_pointcloud_publisher")

    point_cloud = SimPointCloud()

    rospy.spin()


    # rate = rospy.Rate(float(rateParam))
    # while not rospy.is_shutdown():
    #     node.runNode()
    #     rate.sleep()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
