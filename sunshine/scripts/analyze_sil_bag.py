import rosbag
import rospy
import numpy as np

from nav_msgs.msg import Path, MapMetaData
from std_msgs.msg import Float32MultiArray
from sunshine_msgs.msg import DenseTopicMap
import cv2
import matplotlib.pyplot as plt

def missionToPath(mission):
    path = []
    x0, x1, y0, y1 = np.inf, -np.inf, np.inf, -np.inf
    for p in mission.poses:
        # if p.header.frame_id != self.world_frame:
        #     p = self.tf_buffer.transform(p, self.world_frame, rospy.Duration(0))
        x0 = min(x0, p.pose.position.x, x0)
        x1 = max(x1, p.pose.position.x, x1)
        y0 = min(y0, p.pose.position.y, y0)
        y1 = max(y1, p.pose.position.y, y1)
        path.append((p.pose.position.x, p.pose.position.y))
    return np.array(path)

def discretizePath(path, num_points, first_half=False):
    waypoints = path[:len(path)//2] if first_half else path
    diffs = np.concatenate((np.zeros((1,2)), np.diff(waypoints, axis=0)), axis=0)
    dists = np.sqrt(np.sum(np.square(diffs), axis=1))
    waypoint_ts = np.cumsum(dists)
    T = waypoint_ts[-1]
    ts = np.linspace(0, T, num_points - 1, endpoint=False)
    ts = np.concatenate((ts, [T]), axis=0)
    xs = []
    ys = []
    for t in ts:
        xs.append(np.interp(t, waypoint_ts, waypoints[:, 0]))
        ys.append(np.interp(t, waypoint_ts, waypoints[:, 1]))
    return np.concatenate((np.array(xs)[:, np.newaxis], np.array(ys)[:, np.newaxis]), axis=1)

def to_coords(pose, map_info, cell_width):
    """

    :type map_info: MapMetaData
    """
    origin = [map_info.origin.position.x, map_info.origin.position.y]
    new_pose = (pose - np.array(origin)) / np.array(cell_width)[np.newaxis, :2]
    return new_pose.astype(int)


input_file = "/data/2023-06-USVI/2023-07-05/warpauv_1_orin1_2023-07-05-09-26-35.bag"
raw_file = "/data/2023-06-USVI/2023-07-05/warpauv_1_orin1_RAW_2023-07-05-09-26-35.bag"

mission: Path = None
plan = None
history: Float32MultiArray = None
map: DenseTopicMap = None
topic: str
poses = None
coords = None
t: rospy.Time
f = plt.Figure()
ax = f.axes
frame = 0
for topic, msg, t in rosbag.Bag(input_file).read_messages():
    if topic.endswith('/full_mission'):
        if msg == mission:
            continue
        mission = msg
    elif topic.endswith('/sil/history'):
        history = msg
    elif topic.endswith('/sil/map_extrapolator/extrapolated_map'):
        map = msg
        continue
    elif topic.endswith('/sil/plan'):
        plan = msg
        continue  # todo: remove once we use the plan for something
    else:
        continue

    if mission is not None and history is not None:
        poses = discretizePath(missionToPath(mission), len(history.data))
        # print(poses)

    if poses is not None and map is not None:
        map_size = np.array([map.info.width, map.info.height, -1])
        map_topics = np.squeeze(np.array(map.map_topic_proportions).reshape(map_size))
        K = map_topics.shape[-1]
        sl = int(np.ceil(np.sqrt(K)))
        if sl*sl != K:
            map_topics = np.concatenate((map_topics, np.zeros((map_topics.shape[0], map_topics.shape[1], sl*sl-K))), axis=-1)

        # rows = [cv2.hconcat([map_topics[:, :, i+d] for i in range(sl)]) for d in range(0, sl*sl, sl)]
        # tiled = cv2.vconcat(rows)
        tiled = cv2.hconcat([map_topics[:,:,1], map_topics[:,:,1]])

        color = cv2.convertScaleAbs(np.repeat(tiled[:,:,np.newaxis], 3, axis=-1), alpha=255.)
        coords = to_coords(poses, map.info, map.spatial_cell_size)
        for i in range(coords.shape[0] - 1):
            if history.data[i] == 0.:
                color = cv2.drawMarker(color, tuple(coords[i, :]), color=(0,255,255), markerSize=2)
                break
            color = cv2.line(color, tuple(coords[i, :]), tuple(coords[i+1, :]), (255,0,0) if history.data[i] == 1. else (0,0,255))
        display = color

        # print(t.to_sec())
        display = cv2.resize(display, (0, 0), fx=8, fy=8, interpolation=cv2.INTER_NEAREST)
        cv2.imwrite(f'sil{frame:06d}.png', display[::-1,:,:])
        frame += 1
        cv2.imshow('topic_densities', display[::-1,:,:])
        cv2.waitKey(10)