import os
import cv2
import sys
import csv
from pathlib import Path

def read_csv_data(csv_file_name):
    data = {}
    if os.path.isfile(csv_file_name):
        with open(csv_file_name, 'r', newline='') as csv_file:
            csv_reader = csv.reader(csv_file)
            next(csv_reader)  # Skip header
            for row in csv_reader:
                if len(row) == 2:
                    data[row[0]] = row[1]
    return data

def write_csv_data(csv_file_name, data):
    with open(csv_file_name, 'w', newline='') as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow(['Image Filename', 'User Input'])
        for image_file, user_input in data.items():
            csv_writer.writerow([image_file, user_input])

def main(input_directory, csv_file_name):
    image_files = sorted([f for f in os.listdir(input_directory) if f.endswith(('.png', '.jpg', '.jpeg'))])

    image_files = image_files[::5]

    csv_data = read_csv_data(csv_file_name)
    # window_name = 'Image'
    # cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)

    index = 0
    force = False
    while index < len(image_files):
        image_file = image_files[index]

        if force or image_file not in csv_data:
            img = cv2.imread(os.path.join(input_directory, image_file))
            cv2.imshow(image_file, cv2.resize(img, (0,0), fx=0.4, fy=0.4))

            while True:
                key = cv2.waitKey(0)
                if key == ord('1') or key == ord('2') or key == ord('3'):
                    user_input = chr(key)
                    csv_data[image_file] = user_input
                    print(image_file, user_input)
                    write_csv_data(csv_file_name, csv_data)
                    index += 1
                    break
                elif key == 8:  # Backspace key
                    force = True
                    if index > 0:
                        index -= 1
                    break
                elif key == 27:  # Esc key
                    cv2.destroyAllWindows()
                    return
            cv2.destroyAllWindows()
        else:
            index += 1

    cv2.destroyAllWindows()

if __name__ == '__main__':
    input_directory, csv_file_name = sys.argv[1:3]

    input_directory = Path(input_directory)

    if input_directory.is_dir() and len(list(input_directory.glob('*'))) > 0:
        main(input_directory, csv_file_name)
    else:
        print("Invalid directory path or the directory is empty.")
